var employee_select = document.getElementById("user_id");
var selected_employee = "";
var project_employees = [];

$(document).find("#project_id").change(function(){

    //Get project's employees
    var project_employee_request = new XMLHttpRequest();
    project_employee_request.open('GET','/projects/'+this.value+'/employee_assignments');
    project_employee_request.onload = function () {
        project_employees = JSON.parse(project_employee_request.responseText)
            .sort((a, b) => (a.first_name+a.last_name > b.first_name+b.last_name) ? 1 : -1);
        $(document).find("#user_id").prop( "disabled", false );
        $(document).find("#user_id").html("");

        var new_select;
        for (var i = 0; i < project_employees.length; i++) {
            new_select = document.createElement("option");
            new_select.value = project_employees[i].id;
            new_select.innerHTML = project_employees[i].first_name + ' ' + project_employees[i].last_name;
            if(selected_employee === project_employees[i].id.toString()) new_select.selected = true;
            $(document).find("#user_id").append(new_select);
        }
        var empty_select = document.createElement("option");
        empty_select.innerHTML = "Select employee";
        if(!pluck(project_employees,'id').includes(selected_employee)){
            empty_select.selected = true;
            selected_employee = "";
        }
        $(document).find("#user_id").prepend(empty_select);
        empty_select.disabled = true;
    };
    project_employee_request.send();
});

$(document).find("#user_id").change(function(){
    selected_employee = employee_select.options[employee_select.selectedIndex].value;
});
