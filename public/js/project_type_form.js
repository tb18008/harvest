//Billable or Non-billable select switch. Shows one, hides the other
$(document).find('#billable_checkbox').on('change', function() {
    if(this.checked) {
        $("#is_billable").val('1');
        if($("#bill_by").val('none')) $("#rate_input").hide();
        $("#form_billable").show();
        $("#billable_budget_by").show();
        $("#non_billable_budget_by").hide();
    }
    else{
        $("#is_billable").val('0');
        if($("#bill_by").val('none')) $("#rate_input").hide();
        $("#form_billable").hide();
        $("#billable_budget_by").hide();
        $("#non_billable_budget_by").show();
    }
    budgetByChanged();
});

//Fixed fee or Hourly rate switch. Shows one, hides the other
$(document).find("#is_fixed_fee").on('change', function() {
    if(this.value === '1') {
        $("#fixed_fee_input").show();
        $("#hourly_rate_input").hide();
    }
    else{
        $("#fixed_fee_input").hide();
        $("#hourly_rate_input").show();
    }
});

//Only Project Hourly Rate shows a rate input
$(document).find("#bill_by").on('change', function() {
    if(this.value === 'Project') {
        $("#rate_input").show();
    }
    else{
        $("#hourly_rate").val("");
        $("#rate_input").hide();
    }
});

//Budget select option changes call function
$(document).find("#billable_budget_by").on('change',budgetByChanged);
$(document).find("#non_billable_budget_by").on('change',budgetByChanged);

function budgetByChanged() {
    if ($("#billable_checkbox").is(":checked")) $budget_select = $("#billable_budget_by") //Initiate $budget_select variable
    else $budget_select = $("#non_billable_budget_by") //If project is not billable a different select is used

    $("#budget_by").val($($budget_select, 'option:selected').val());   //Change hidden input value for request data

    //Hiding/Showing input based on select values. Only budget in hours is posted to harvest
    if($($budget_select, 'option:selected').val() !== 'project'){
        $("#budget_input").hide();
        return;
    }
    $("#budget_input").show();
}
