$(document).ready(function(){
    $(document).find( "#date_input" ).datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(dateText){
            $(document).find("#spent_date").val(dateText+"T00:00:00+0000");
        }
    });
});
