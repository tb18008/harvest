var task_select = document.getElementById("task_id");
var selected_task = "";
var project_tasks = [];

$(document).find("#project_id").change(function(){
    //Get project's tasks
    var project_task_request = new XMLHttpRequest();
    project_task_request.open('GET','/projects/'+this.value+'/task_assignments');
    project_task_request.onload = function () {
        project_tasks = JSON
            .parse(project_task_request.responseText)
            .sort((a, b) => (a.name > b.name) ? 1 : -1);
        $(document).find("#task_id").prop( "disabled", false );
        $(document).find("#task_id").html("");

        var new_select;
        for (var i = 0; i < project_tasks.length; i++) {
            new_select = document.createElement("option");
            new_select.value = project_tasks[i].id;
            new_select.innerHTML = project_tasks[i].name;
            if(selected_task === project_tasks[i].id.toString()) new_select.selected = true;
            $(document).find("#task_id").append(new_select);
        }
        var empty_select = document.createElement("option");
        empty_select.innerHTML = "Select task";
        if(!pluck(project_tasks,'id').includes(selected_task)){
            empty_select.selected = true;
            selected_task = "";
        }
        $(document).find("#task_id").prepend(empty_select);
        empty_select.disabled = true;
    };
    project_task_request.send();
});
$(document).find("#task_id").change(function(){
    selected_task = task_select.options[task_select.selectedIndex].value;
});
