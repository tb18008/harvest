<?php


namespace App;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AssignmentService
{
    public $harvest_service;

    public function __construct()
    {
        $this->harvest_service = new HarvestService();
    }

    public function assign($assignee_model, array $related_model_names, array $request_data){
        foreach ($related_model_names as $related_model_name){
            $form_field_name = config('harvest.'.$related_model_name.'_assignments');
            if(array_key_exists($form_field_name,$request_data)){    //User selected at least one related model
                $user_input_data = collect($request_data[$form_field_name]);
            }
            else{
                $user_input_data = collect([]);
            }
            $assignment_response = $this->matchAssignment($assignee_model, $user_input_data,$related_model_name);
            if($assignment_response) return redirect()->back()->with('message',$assignment_response);
        }

        return null;
    }

    private function matchAssignment($assignee_model, $user_input_data, $model_name){
        $assignment_type = $model_name.'_assignments';
        $harvest_model = config('harvest.'.$model_name);

        //Get Harvest assignment list
        $harvest_model_assignments = $this->getHarvestAssignments($assignee_model,$assignment_type,$model_name);

        //MATCH HARVEST WITH USER INPUT

        //Delete those in Harvest, but not in assigned_models
        $deletable_model_assignments = $harvest_model_assignments
            ->filter(function ($model_assignment) use ($user_input_data, $model_name){
                return !$user_input_data->contains($model_assignment[$model_name.'_id']); //Collection of model assignments that aren't in the user assigned models
            })
            ->pluck('id')
            ->toArray();

        foreach ($deletable_model_assignments as $deletable_id){
            $response = $this->harvest_service
                ->deleteAssignment(
                    config('harvest.'.get_class($assignee_model).'_plur'),
                    $assignee_model->id,
                    $assignment_type,
                    $deletable_id
                );
            if(!is_array($response)) return redirect()->back()->with('message',$response);
        }

        //Insert those in assigned_models, but not in Harvest

        $insertable_model_ids = $user_input_data
            ->diff($harvest_model_assignments
                ->pluck($harvest_model.'_id'))
            ->toArray();

        foreach ($insertable_model_ids as $insertable_model_id){
            $response = $this->harvest_service
                ->createAssignment(
                    config('harvest.'.get_class($assignee_model).'_plur'),
                    $assignee_model->id,
                    $assignment_type,
                    [$harvest_model.'_id' => $insertable_model_id]
                );
            if(!is_array($response)) return redirect()->back()->with('message',$response);
        }

        //MATCH DB WITH HARVEST

        //Get Harvest's new assignment list
        $harvest_model_assignments = $this->getHarvestAssignments($assignee_model,$assignment_type,$model_name);

        $db_model_assignments = DB::table(config('harvest.table_'.$assignment_type))
            ->where(config('harvest.'.get_class($assignee_model).'_sing').'_id',$assignee_model->id)
            ->pluck('id');//Entity assignments according to database

        $deletable_model_assignments = $db_model_assignments
            ->filter(function ($model_assignment) use ($harvest_model_assignments){
                return !$harvest_model_assignments
                    ->pluck('id')
                    ->contains($model_assignment);
            }); //Collection of model assignments that aren't in the user assigned models

        if($deletable_model_assignments->isNotEmpty()) {
            DB::table(config('harvest.table_'.$assignment_type))
                ->whereIn('id',$deletable_model_assignments->toArray())
                ->delete();
        }

        $insertable_model_ids = $harvest_model_assignments->pluck('id')->diff($db_model_assignments->toArray());

        if($insertable_model_ids->isNotEmpty()){
            $insertable_models = $harvest_model_assignments
                ->whereIn('id',$insertable_model_ids)
                ->map(function ($insertable_model) use ($assignee_model) {
                    return collect($insertable_model)
                        ->put(config('harvest.'.get_class($assignee_model).'_sing').'_id',$assignee_model->id)
                        ->toArray();
                });
            foreach ($insertable_models as $insertable_model){
                DB::table(config('harvest.table_'.$assignment_type))->insert($insertable_model);
            }

        }

        return null;

    }

    private function getHarvestAssignments($assignee_model, $assignment_type, $model_name){
        $harvest_model = config('harvest.'.$model_name);

        return collect($this
            ->harvest_service
            ->getObjectAssignments(
                config('harvest.'.get_class($assignee_model).'_plur'),
                $assignee_model->id,
                config('harvest.'.$assignment_type)
            )[config('harvest.'.$assignment_type)])
            ->map(function ($model_assignment) use ($harvest_model, $model_name){
                return [
                    'id' => $model_assignment['id'],
                    $model_name.'_id' => $model_assignment[$harvest_model]['id'],
                ];
            }); //Assignee's assignments from Harvest formatted with their id and model id
    }
}
