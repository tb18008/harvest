<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{

    protected $fillable = ['id','name', 'client_id','is_active','is_billable', 'is_fixed_fee', 'fee', 'hourly_rate_type_id', 'hourly_rate', 'budget_type_id', 'budget'];

    public function expenses()
    {
        return $this->hasMany('App\Expense');
    }

    public function time_entries()
    {
        return $this->hasMany('App\TimeEntry');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function hourly_rate_type()
    {
        return $this->belongsTo('App\HourlyRateType');
    }

    public function budget_type()
    {
        return $this->belongsTo('App\BudgetType');
    }

    public function tasks()
    {
        return $this->belongsToMany('App\Task');
    }

    public function employees()
    {
        return $this->belongsToMany('App\Employee');
    }

    public function invoice_line_items()
    {
        return $this->hasMany('App\InvoiceLineItem');
    }
}
