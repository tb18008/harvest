<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    protected $fillable = ['id','name'];

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

    public function time_entries()
    {
        return $this->hasMany('App\TimeEntry');
    }
}
