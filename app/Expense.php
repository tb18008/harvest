<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['id', 'employee_id', 'project_id', 'expense_category_id', 'spent_date', 'billable', 'total_cost'];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function expense_category()
    {
        return $this->belongsTo('App\ExpenseCategory');
    }

    public function getNameAttribute()
    {
        return $this->id;
    }
}
