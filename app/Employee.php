<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Employee extends Model
{

    protected $fillable = ['id','first_name', 'last_name', 'email', 'cost_rate','slack_webhook'];

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

    public function time_entries()
    {
        return $this->hasMany('App\TimeEntry');
    }

    public function getNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */

    public function routeNotificationForSlack($notification)
    {
        return $this->slack_webhook;
    }

    use Notifiable;
}
