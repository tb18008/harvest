<?php

namespace App\Console;

use App\Events\EmployeeNotificationEvents\CheckUnfilledTimesheetsEvent;
use App\Events\RemoveDuplicateJobsEvent;
use App\Events\RoutineUpdateEvents\UpdateAllEntitiesEvent;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Routine Harvest update
        $schedule->call(function () {
            event(new UpdateAllEntitiesEvent());
        })->daily();
        $schedule->call(function () {
            event(new CheckUnfilledTimesheetsEvent());
        })->dailyAt('18:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
