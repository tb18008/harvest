<?php

namespace App\Events\InitialStoreEvents;

use App\HarvestService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StoreEmployeeAssignmentsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $harvest_json;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($page)
    {
        $harvest_service = new HarvestService();
        $this->harvest_json = $harvest_service->getObjects('employee_assignments', $page);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
