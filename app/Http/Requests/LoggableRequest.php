<?php

namespace App\Http\Requests;

use App\Jobs\LogRequestJob;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class LoggableRequest extends FormRequest
{
    protected function failedAuthorization()
    {
        $request_data = [
            'url' => $this->url(),
            'request' => collect($this->request->all())->toJson(),
            'method' => $this->method(),
            'status' => 'fail',
            'user_id' => $this->user()->id,
            'authorized' => false,
            'errors' => null,
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        LogRequestJob::dispatchNow($request_data, true);

        throw new AuthorizationException;
    }

    protected function passedValidation()
    {
        $request_data = [
            'url' => $this->url(),
            'request' => collect($this->request->all())->toJson(),
            'method' => $this->method(),
            'status' => 'success',
            'user_id' => $this->user()->id,
            'authorized' => true,
            'errors' => null,
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        LogRequestJob::dispatchNow($request_data, true);
    }

    protected function failedValidation(Validator $validator)
    {
        $validation_exception = new ValidationException($validator);

        $request_data = [
            'url' => $this->url(),
            'request' => collect($this->request->all())->toJson(),
            'method' => $this->method(),
            'status' => 'fail',
            'authorized' => true,
            'user_id' => $this->user()->id,
            'errors' => collect($validation_exception->errors())->toJson(),
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        LogRequestJob::dispatchNow($request_data, true);

        throw $validation_exception
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
