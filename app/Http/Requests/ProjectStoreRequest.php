<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class ProjectStoreRequest extends LoggableRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required|numeric|exists:clients,id',
            'name' => 'required|string|max:191|unique:projects,name',
            'is_billable' => 'boolean',
            'is_fixed_fee' => 'boolean',
            'fee' => 'numeric|min:0|nullable',
            'bill_by' => 'required|string',Rule::in(['none','Project','Tasks','People']),
            'hourly_rate' => 'numeric|min:0|nullable',
            'budget_by' => 'required|string',Rule::in(['none','project_cost','task','project','task_fees','person']),
            'budget' => 'numeric|min:0|nullable',
            'task_assignments' => 'array|nullable',
            'task_assignments.*' => 'numeric|exists:tasks,id|nullable',
            'user_assignments' => 'array|nullable',
            'user_assignments.*' => 'numeric|exists:employees,id|nullable',
        ];
    }
}
