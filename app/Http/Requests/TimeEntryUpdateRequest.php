<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TimeEntryUpdateRequest extends LoggableRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required|numeric|exists:projects,id',
            'task_id' => 'required|numeric|exists:tasks,id',
            'hours' => 'numeric|min:0|nullable',
            'spent_date' => 'required|date|date_format:"Y-m-d\TH:i:sO"',
        ];
    }
}
