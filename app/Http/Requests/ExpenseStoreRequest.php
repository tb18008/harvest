<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpenseStoreRequest extends LoggableRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable|numeric|exists:employees,id',
            'project_id' => 'required|numeric|exists:projects,id',
            'expense_category_id' => 'required|numeric|exists:expense_categories,id',
            'spent_date' => 'required|date|date_format:"Y-m-d\TH:i:sO"',
            'billable' => 'boolean',
            'total_cost' => 'numeric|min:0|nullable',
        ];
    }
}
