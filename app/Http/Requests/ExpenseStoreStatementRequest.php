<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpenseStoreStatementRequest extends LoggableRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expenses' => 'array',
            'expenses.*.include' => 'boolean',
            'expenses.*.spent_date' => 'required|date|date_format:"Y-m-d\TH:i:sO"',
            'expenses.*.total_cost' => 'numeric|min:0|nullable',
            'expenses.*.client_name' => 'nullable|string',
            'expenses.*.user_id' => 'required_if:expenses.*.include,true|numeric|exists:employees,id',
            'expenses.*.project_id' => 'required_if:expenses.*.include,true|numeric|exists:projects,id',
            'expenses.*.expense_category_id' => 'required_if:expenses.*.include,true|numeric|exists:expense_categories,id',
            'expenses.*.billable' => 'required_if:expenses.*.include,true|boolean',
            'expenses.*.default' => 'boolean',
        ];
    }
}
