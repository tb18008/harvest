<?php

namespace App\Http\Controllers;

use App\AssignmentService;
use App\BudgetType;
use App\Employee;
use App\HarvestService;
use App\HourlyRateType;
use App\Http\Requests\ProjectToggleActiveRequest;
use App\Project;
use App\Client;
use App\Http\Requests\ProjectUpdateRequest;
use App\Http\Requests\ProjectStoreRequest;
use App\Task;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::with(['client','budget_type', 'hourly_rate_type'])->paginate(13);
        return view('project.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all()->sortBy('name');
        $tasks = Task::all()->sortBy('name');
        $employees = Employee::all()->sortBy(function ($employee){
            return $employee->first_name . $employee->last_name;
        });
        return view('project.create',compact(['clients','tasks','employees']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectStoreRequest $request)
    {
        $harvest = new HarvestService();
        $new_project = $harvest->createObject('projects', $request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }

        $assignment_service = new AssignmentService();
        $assignment_service->assign($new_project,['task', 'employee'], $request->validated());

        return redirect('/projects')->with('message',$harvest->message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $clients = Client::all()->sortBy('name');
        $tasks = Task::all()->sortBy('name');
        $employees = Employee::all()->sortBy(function ($employee){
            return $employee->first_name . $employee->last_name;
        });
        $project = Project::with(['tasks','employees'])->find($project->id);

        return view('project.edit',compact(['project','clients','tasks','employees']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProjectUpdateRequest $request
     * @param Project $project
     * @return void
     */
    public function update(ProjectUpdateRequest $request, Project $project)
    {
        $harvest = new HarvestService();
        $updated_project = $harvest->updateObject('projects',$project->id,$request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }

        $assignment_service = new AssignmentService();
        $assignment_service->assign($updated_project,['task', 'employee'], $request->validated());

        return redirect('/projects')->with('message',$harvest->message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return void
     */
    public function destroy(Project $project)
    {
        $harvest = new HarvestService();

        $project->time_entries()->delete();
        $project->tasks()->detach();
        $project->employees()->detach();

        $harvest->deleteObject('projects',$project->id);

        return redirect()->back()->with('message',$harvest->message);
    }

    public function toggleActive(ProjectToggleActiveRequest $request, Project $project)
    {
        if($project->is_active == $request->validated()['is_active']) return redirect()->back();

        $harvest = new HarvestService();
        $harvest->updateObject('projects',$project->id,$request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }

        return redirect()->back()->with('message',$harvest->message);
    }
}
