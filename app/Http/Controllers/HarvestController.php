<?php

namespace App\Http\Controllers;

use App\Events\InitialStoreEvents\StoreAllEntitiesEvent;
use App\Events\RoutineUpdateEvents\UpdateAllEntitiesEvent;
use Illuminate\Http\Request;

class HarvestController extends Controller
{
    public function store()
    {
        event(new StoreAllEntitiesEvent());
        return back();
    }

    public function update()
    {
        event(new UpdateAllEntitiesEvent());
        return back();
    }
}
