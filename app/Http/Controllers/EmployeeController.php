<?php

namespace App\Http\Controllers;

use App\Employee;
use App\HarvestService;
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\SlackWebhookService;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::paginate(13);
        return view('employee.index',compact('employees'));
    }
    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(EmployeeStoreRequest $request)
    {
        $harvest = new HarvestService();
        $harvest->createObject('employees', $request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }

        return redirect('/employees')->with('message',$harvest->message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Employee $employee
     */
    public function edit(Employee $employee)
    {
        return view('employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Employee $employee
     */
    public function update(EmployeeUpdateRequest $request, Employee $employee)
    {
        $harvest = new HarvestService();
        $harvest->updateObject('employees',$employee->id,$request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }
        return redirect('/employees')->with('message',$harvest->message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Employee $employee
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Employee $employee)
    {
        if($employee
            ->time_entries()
            ->get()
            ->isNotEmpty()
        ) return redirect()
            ->back()
            ->with('message', __('messages.del_employee_has_time_entries', ['first_name'=> $employee->first_name, 'last_name'=>$employee->last_name]));

        $harvest = new HarvestService();
        $harvest->deleteObject('employees',$employee->id);

        return redirect()->back()->with('message',$harvest->message);
    }
}
