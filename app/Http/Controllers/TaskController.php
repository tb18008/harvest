<?php

namespace App\Http\Controllers;

use App\HarvestService;
use App\Http\Requests\TaskStoreRequest;
use App\Http\Requests\TaskUpdateRequest;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::paginate(13);
        return view('task.index',compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskStoreRequest $request)
    {
        $harvest = new HarvestService();
        $harvest->createObject('tasks', $request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }

        return redirect('/tasks')->with('message',$harvest->message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        return view('task.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskUpdateRequest $request
     * @param Task $task
     * @return void
     */
    public function update(TaskUpdateRequest $request, Task $task)
    {
        $harvest = new HarvestService();
        $harvest->updateObject('tasks',$task->id,$request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }
        return redirect('/tasks')->with('message',$harvest->message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return void
     */
    public function destroy(Task $task)
    {
        if($task
            ->time_entries()
            ->get()
            ->isNotEmpty()
        ) return redirect()
            ->back()
            ->with('message', __('messages.del_task_has_time_entries', ['name'=> $task->name]));

        $harvest = new HarvestService();
        $harvest->deleteObject('tasks',$task->id);

        return redirect()->back()->with('message',$harvest->message);
    }
}
