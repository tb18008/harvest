<?php

namespace App\Http\Controllers;

use App\Client;
use App\ReportService;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function show($report_type, Request $request){
        $report_service = new ReportService();
        switch ($report_type) {
            case 'timetable':
                if($request->period_select == null) $request->period_select = 'all';
                $report = $report_service->getTimetable($request->period_select, $request->prev_next, $request->start_date, $request->end_date);
                return view('report.show',compact('report'));
            case 'net_income':
                if($request->period_select == null) $request->period_select = 'all';
                $report = $report_service->getNetIncome($request->period_select, $request->prev_next, $request->start_date, $request->end_date);
//                dd($report);
                return view('report.show',compact('report'));
            default:
                return back();
        }
    }
}
