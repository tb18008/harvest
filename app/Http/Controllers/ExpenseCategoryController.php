<?php

namespace App\Http\Controllers;

use App\ExpenseCategory;
use App\HarvestService;
use App\Http\Requests\ExpenseCategoryStoreRequest;
use App\Http\Requests\ExpenseCategoryUpdateRequest;
use Illuminate\Http\Request;

class ExpenseCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expense_categories = ExpenseCategory::paginate(13);
        return view('expense_category.index',compact('expense_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('expense_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseCategoryStoreRequest $request)
    {
        $harvest = new HarvestService();
        $harvest->createObject('expense_categories', $request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }

        return redirect('/expense_categories')->with('message',$harvest->message);
    }

    /**
     * Display the specified resource.
     *
     * @param ExpenseCategory $expense_category
     * @return void
     */
    public function show(ExpenseCategory $expense_category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ExpenseCategory $expense_category
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpenseCategory $expense_category)
    {
        return view('expense_category.edit', compact('expense_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param ExpenseCategory $expense_category
     * @return void
     */
    public function update(ExpenseCategoryUpdateRequest $request, ExpenseCategory $expense_category)
    {
        $harvest = new HarvestService();
        $harvest->updateObject('expense_categories',$expense_category->id,$request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }
        return redirect('/expense_categories')->with('message',$harvest->message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpenseCategory  $expense_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpenseCategory $expense_category)
    {
        if($expense_category
            ->expenses()
            ->get()
            ->isNotEmpty()
        ) return redirect()
            ->back()
            ->with('message', __('messages.del_expense_category_has_expenses', ['name'=> $expense_category->name]));

        $harvest = new HarvestService();
        $harvest->deleteObject('expense_categories',$expense_category->id);

        return redirect()->back()->with('message',$harvest->message);
    }
}
