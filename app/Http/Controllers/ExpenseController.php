<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Expense;
use App\ExpenseCategory;
use App\HarvestService;
use App\Http\Requests\ExpenseStoreRequest;
use App\Http\Requests\ExpenseStoreStatementRequest;
use App\Http\Requests\ExpenseUpdateRequest;
use App\ImportDefault;
use App\ImportDefaultsService;
use App\Project;
use App\StatementEntry;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::with(['employee','project', 'expense_category'])->paginate(13);
        return view('expense.index',compact('expenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all()->sortBy(function ($employee){
            return $employee->first_name . $employee->last_name;
        });
        $projects = Project::all()->sortBy('name');
        $expense_categories = ExpenseCategory::all()->sortBy('name');
        return view('expense.create', compact(['employees', 'projects', 'expense_categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseStoreRequest $request)
    {
        $harvest = new HarvestService();
        $harvest->createObject('expenses', $request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }

        return redirect('/expenses')->with('message',$harvest->message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Expense $expense)
    {
        $employees = Employee::all()->sortBy(function ($employee){
            return $employee->first_name . $employee->last_name;
        });
        $projects = Project::all()->sortBy('name');
        $expense_categories = ExpenseCategory::all()->sortBy('name');
        return view('expense.edit', compact(['expense','employees', 'projects', 'expense_categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseUpdateRequest $request, Expense $expense)
    {
        $harvest = new HarvestService();
        $harvest->updateObject('expenses',$expense->id,$request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }
        return redirect('/expenses')->with('message',$harvest->message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        $harvest = new HarvestService();
        $harvest->deleteObject('expenses',$expense->id);

        return redirect()->back()->with('message',$harvest->message);
    }

    public function create_from_statement($statement_id){

        $import_defaults_service = new ImportDefaultsService();
        $statement_entries = $import_defaults_service->assignDefaultValues(StatementEntry::where('statement_id',$statement_id)->get());
        $employees = Employee::all()->sortBy(function ($employee){
            return $employee->first_name . $employee->last_name;
        });
        $projects = Project::all()->sortBy('name');
        $expense_categories = ExpenseCategory::all();
        return view('statement.create',compact(['statement_entries','employees','projects','expense_categories']));
    }

    public function store_from_statement(ExpenseStoreStatementRequest $request){

        $harvest = new HarvestService();

        foreach (collect($request->validated()['expenses'])->where('include', 1) as $expense){  //Only included expenses
            $harvest->createObject('expenses', $expense);

            if ($request->session()->has('message.error')) {
                return redirect()->back()->with('message',$harvest->message)->withInput();
            }
        }

        $import_defaults_service = new ImportDefaultsService();
        $import_defaults_service->storeDefaultValues(collect($request->validated()['expenses'])->where('default', 1));

        return redirect('/expenses')->with('message',$harvest->message);
    }
}
