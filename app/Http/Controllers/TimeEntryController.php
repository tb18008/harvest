<?php

namespace App\Http\Controllers;

use App\Employee;
use App\HarvestService;
use App\Http\Requests\TimeEntryStoreRequest;
use App\Http\Requests\TimeEntryUpdateRequest;
use App\Project;
use App\Task;
use App\TimeEntry;
use Illuminate\Http\Request;

class TimeEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $time_entries = TimeEntry::with(['employee','project', 'task'])->paginate(13);
        return view('time_entry.index',compact('time_entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all()->sortBy(function ($employee){
            return $employee->first_name . $employee->last_name;
        });
        $projects = Project::all()->sortBy('name');
        return view('time_entry.create', compact(['employees', 'projects']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TimeEntryStoreRequest $request)
    {
        $harvest = new HarvestService();
        $harvest->createObject('time_entries', $request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }

        return redirect('/time_entries')->with('message',$harvest->message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TimeEntry $time_entry
     * @return \Illuminate\Http\Response
     */
    public function edit(TimeEntry $time_entry)
    {
        $employees = Employee::all()->sortBy(function ($employee){
            return $employee->first_name . $employee->last_name;
        });
        $projects = Project::all()->sortBy('name');
        $tasks = Task::all();
        return view('time_entry.edit', compact(['time_entry','employees', 'projects', 'tasks']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TimeEntryUpdateRequest $request
     * @param TimeEntry $time_entry
     * @return void
     */
    public function update(TimeEntryUpdateRequest $request, TimeEntry $time_entry)
    {
        $harvest = new HarvestService();
        $harvest->updateObject('time_entries',$time_entry->id,$request->validated());

        if ($request->session()->has('message.error')) {
            return redirect()->back()->with('message',$harvest->message)->withInput();
        }
        return redirect('/time_entries')->with('message',$harvest->message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param TimeEntry $time_entry
     * @return void
     */
    public function destroy(TimeEntry $time_entry)
    {
        $harvest = new HarvestService();
        $harvest->deleteObject('time_entries',$time_entry->id);

        return redirect()->back()->with('message',$harvest->message);
    }
}
