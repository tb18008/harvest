<?php

namespace App\Http\Controllers;

use App\Employee;
use App\ExpenseCategory;
use App\Http\Requests\StatementStoreRequest;
use App\Project;
use App\StatementEntry;
use App\StatementService;
use Illuminate\Http\Request;

class StatementController extends Controller
{
    public function store(StatementStoreRequest $request){
        $statement_service = new StatementService();
        $statement_id = $statement_service->import($request->validated()['user_id'], $request->validated()['csv_file']);
        return redirect('expenses_from_statement/'.$statement_id);
    }
}
