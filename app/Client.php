<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    protected $fillable = ['id','name'];

    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }
}
