<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceLineItem extends Model
{
    protected $fillable = ['id', 'project_id','amount'];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

    public function project()
    {
        return $this->hasOne('App\Project');
    }
}
