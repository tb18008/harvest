<?php

namespace App\Notifications;

use App\Employee;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class UnfilledTimesheet extends Notification
{
    use Queueable;

    private $notification_receiver;
    private $notification_message;

    /**
     * Create a new notification instance.
     *
     * @param Employee $employee
     */
    public function __construct(Employee $employee, $date)
    {
        $this->notification_receiver = $employee->slack_username;
        $this->notification_message = $employee->first_name.', please fill timesheet for '.Carbon::parse($date)->format('Y-m-d').'!';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\SlackMessage
     */
    public function toSlack($notifiable)
    {
        return (new SlackMessage)
            ->from('Harvest Bot', ':calendar:')
            ->content($this->notification_message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
