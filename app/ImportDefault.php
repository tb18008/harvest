<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportDefault extends Model
{
    protected $fillable = ['id','client_name', 'user_id','project_id','expense_category_id', 'billable', 'include'];
}
