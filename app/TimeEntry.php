<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeEntry extends Model
{
    protected $fillable = ['id', 'employee_id', 'project_id', 'task_id', 'hours', 'spent_date'];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function task()
    {
        return $this->belongsTo('App\Task');
    }

    public function getNameAttribute()
    {
        return $this->id;
    }
}
