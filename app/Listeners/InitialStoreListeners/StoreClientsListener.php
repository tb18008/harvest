<?php

namespace App\Listeners\InitialStoreListeners;

use App\Client;
use App\Events\InitialStoreEvents\StoreClientsEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreClientsListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreClientsEvent  $event
     * @return void
     */
    public function handle(StoreClientsEvent $event)
    {   //Looking up new Harvest employees and storing them in DB
        //LOOKUP
        $harvest_clients = collect($event->harvest_json[config('harvest.clients')]);

        $new_clients = $harvest_clients
            ->whereIn('id',$harvest_clients   //Gets Harvest client models ..
            ->pluck('id')
                ->diff(\App\Client::forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new employee data
        foreach($new_clients as $new_client) {
            $data[] = [
                'id' => $new_client['id'],
                'name' => $new_client['name'],
                'created_at' => Carbon::parse($new_client['created_at']),
                'updated_at' => Carbon::parse($new_client['updated_at']),
            ];
        }
        Client::insert($data);    //Store all new employees
    }
}
