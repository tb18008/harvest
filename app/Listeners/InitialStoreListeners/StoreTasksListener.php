<?php

namespace App\Listeners\InitialStoreListeners;

use App\Events\InitialStoreEvents\StoreTasksEvent;
use App\Task;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreTasksListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreTasksEvent  $event
     * @return void
     */
    public function handle(StoreTasksEvent $event)
    {   //Looking up new Harvest tasks and storing them in DB
        //LOOKUP
        $harvest_tasks = collect($event->harvest_json[config('harvest.tasks')]);

        $new_tasks = $harvest_tasks
            ->whereIn('id',$harvest_tasks   //Gets Harvest task models ..
            ->pluck('id')
                ->diff(\App\Task::forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new employee data
        foreach($new_tasks as $new_task) {
            $data[] = [
                'id' => $new_task['id'],
                'name' => $new_task['name'],
                'created_at' => Carbon::parse($new_task['created_at']),
                'updated_at' => Carbon::parse($new_task['updated_at']),
            ];
        }
        Task::insert($data);    //Store all new employees
    }
}
