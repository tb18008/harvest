<?php

namespace App\Listeners\InitialStoreListeners;

use App\Events\InitialStoreEvents\StoreTaskAssignmentsEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class StoreTaskAssignmentsListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreTaskAssignmentsEvent  $event
     * @return void
     */
    public function handle(StoreTaskAssignmentsEvent $event)
    {   //Looking up new Harvest task assignments and storing them in DB
        //LOOKUP
        $harvest_task_assignments = collect($event->harvest_json[config('harvest.task_assignments')]);

        $new_task_assignments = $harvest_task_assignments
            ->whereIn('id',$harvest_task_assignments   //Gets Harvest task assignment models ..
            ->pluck('id')
                ->diff(DB::table('project_task')->forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new task assignment data
        foreach($new_task_assignments as $new_task_assignment) {
            $data[] = [
                'id' => $new_task_assignment['id'],
                'project_id' => $new_task_assignment['project']['id'],
                'task_id' => $new_task_assignment['task']['id'],
                'created_at' => Carbon::parse($new_task_assignment['created_at']),
                'updated_at' => Carbon::parse($new_task_assignment['updated_at']),
            ];
        }
        DB::table('project_task')->insert($data);    //Store all new employees
    }
}
