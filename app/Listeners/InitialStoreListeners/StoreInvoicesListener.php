<?php

namespace App\Listeners\InitialStoreListeners;

use App\Events\InitialStoreEvents\StoreInvoicesEvent;
use App\Invoice;
use App\InvoiceLineItem;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreInvoicesListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreInvoicesEvent  $event
     * @return void
     */
    public function handle(StoreInvoicesEvent $event)
    {   //Looking up new Harvest invoices and storing them in DB
        //LOOKUP
        $harvest_invoices = collect($event->harvest_json[config('harvest.invoices')]);

        $new_invoices = $harvest_invoices
            ->whereIn('id',$harvest_invoices   //Gets Harvest invoice models ..
            ->pluck('id')
                ->diff(\App\Invoice::forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new invoice data
        $invoice_line_items = [];

        foreach($new_invoices as $new_invoice) {
            $data[] = [
                'id' => $new_invoice['id'],
                'client_id' => $new_invoice['client']['id'],
                'amount' => $new_invoice['amount'],
                'issue_date' => $new_invoice['issue_date'],
                'created_at' => Carbon::parse($new_invoice['created_at']),
                'updated_at' => Carbon::parse($new_invoice['updated_at']),
            ];
            foreach($new_invoice['line_items'] as $line_item){
                if($line_item['project']) $line_item['project'] = $line_item['project']['id'];
                $invoice_line_items[] = [
                    'id' => $line_item['id'],
                    'invoice_id' => $new_invoice['id'],
                    'project_id' => $line_item['project'],
                    'amount' => $line_item['amount'],
                ];
            }
        }
        Invoice::insert($data);    //Store all new invoices
        InvoiceLineItem::insert($invoice_line_items);    //Store all new invoice line items
    }
}
