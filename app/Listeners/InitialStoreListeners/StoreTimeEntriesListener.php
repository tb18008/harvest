<?php

namespace App\Listeners\InitialStoreListeners;

use App\Events\InitialStoreEvents\StoreTimeEntriesEvent;
use App\TimeEntry;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreTimeEntriesListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreTimeEntriesEvent  $event
     * @return void
     */
    public function handle(StoreTimeEntriesEvent $event)
    {   //Looking up new Harvest time entries and storing them in DB
        //LOOKUP
        $harvest_time_entries = collect($event->harvest_json[config('harvest.time_entries')]);

        $new_time_entries = $harvest_time_entries
            ->whereIn('id',$harvest_time_entries   //Gets Harvest time entry models ..
            ->pluck('id')
                ->diff(\App\TimeEntry::forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new employee data
        foreach($new_time_entries as $new_time_entry) {
            $data[] = [
                'id' => $new_time_entry['id'],
                'employee_id' => $new_time_entry['user']['id'],
                'project_id' => $new_time_entry['project']['id'],
                'hours' => $new_time_entry['hours'],
                'spent_date' => $new_time_entry['spent_date'],
                'task_id' => $new_time_entry['task']['id'],
                'created_at' => Carbon::parse($new_time_entry['created_at']),
                'updated_at' => Carbon::parse($new_time_entry['updated_at']),
            ];
        }
        TimeEntry::insert($data);    //Store all new employees
    }
}
