<?php

namespace App\Listeners\InitialStoreListeners;

use App\BudgetType;
use App\Events\InitialStoreEvents\StoreProjectsEvent;
use App\HourlyRateType;
use App\Project;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreProjectsListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreProjectsEvent  $event
     * @return void
     */
    public function handle(StoreProjectsEvent $event)
    {   //Looking up new Harvest projects and storing them in DB
        //LOOKUP
        $harvest_projects = collect($event->harvest_json[config('harvest.projects')]);

        $new_projects = $harvest_projects
            ->whereIn('id',$harvest_projects   //Gets Harvest project models ..
            ->pluck('id')
                ->diff(\App\Project::forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new employee data
        foreach($new_projects as $new_project) {
            $data[] = [
                'id' => $new_project['id'],
                'client_id' => $new_project['client']['id'],
                'name' => $new_project['name'],
                'budget_type_id' => BudgetType::where('type', $new_project['budget_by'])->first()->id,
                'budget' => $new_project['budget'],
                'hourly_rate_type_id' => HourlyRateType::where('type',$new_project['bill_by'])->first()->id ,
                'hourly_rate' => $new_project['hourly_rate'],
                'is_active' => $new_project['is_active'],
                'is_billable' => $new_project['is_billable'],
                'is_fixed_fee' => $new_project['is_fixed_fee'],
                'fee' => $new_project['fee'],
                'created_at' => Carbon::parse($new_project['created_at']),
                'updated_at' => Carbon::parse($new_project['updated_at']),
            ];
        }
        Project::insert($data);    //Store all new employees
    }
}
