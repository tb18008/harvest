<?php

namespace App\Listeners\InitialStoreListeners;

use App\Employee;
use App\Events\InitialStoreEvents\StoreEmployeesEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreEmployeesListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreEmployeesEvent  $event
     * @return void
     */
    public function handle(StoreEmployeesEvent $event)
    {   //Looking up new Harvest employees and storing them in DB
        //LOOKUP
        $harvest_employees = collect($event->harvest_json[config('harvest.employees')]);

        $new_employees = $harvest_employees
            ->whereIn('id',$harvest_employees   //Gets Harvest employee models ..
            ->pluck('id')
                ->diff(\App\Employee::forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new employee data
        foreach($new_employees as $new_employee) {
            $data[] = [
                'id' => $new_employee['id'],
                'first_name' => $new_employee['first_name'],
                'last_name' => $new_employee['last_name'],
                'email' => $new_employee['email'],
                'cost_rate' => $new_employee['cost_rate'],
                'created_at' => Carbon::parse($new_employee['created_at']),
                'updated_at' => Carbon::parse($new_employee['updated_at']),
            ];
        }
        Employee::insert($data);    //Store all new employees
    }
}
