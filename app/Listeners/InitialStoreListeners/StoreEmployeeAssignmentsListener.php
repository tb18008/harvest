<?php

namespace App\Listeners\InitialStoreListeners;

use App\Events\InitialStoreEvents\StoreEmployeeAssignmentsEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class StoreEmployeeAssignmentsListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreEmployeeAssignmentsEvent  $event
     * @return void
     */
    public function handle(StoreEmployeeAssignmentsEvent $event)
    {   //Looking up new Harvest employee assignments and storing them in DB
        //LOOKUP
        $harvest_employee_assignments = collect($event->harvest_json[config('harvest.employee_assignments')]);

        $new_employee_assignments = $harvest_employee_assignments
            ->whereIn('id',$harvest_employee_assignments   //Gets Harvest employee assignment models ..
            ->pluck('id')
                ->diff(DB::table('employee_project')->forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new employee assignment data
        foreach($new_employee_assignments as $new_employee_assignment) {
            $data[] = [
                'id' => $new_employee_assignment['id'],
                'project_id' => $new_employee_assignment['project']['id'],
                'employee_id' => $new_employee_assignment['user']['id'],
                'created_at' => Carbon::parse($new_employee_assignment['created_at']),
                'updated_at' => Carbon::parse($new_employee_assignment['updated_at']),
            ];
        }
        DB::table('employee_project')->insert($data);    //Store all new employees
    }
}
