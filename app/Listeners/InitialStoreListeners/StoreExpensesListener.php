<?php

namespace App\Listeners\InitialStoreListeners;

use App\Events\InitialStoreEvents\StoreExpensesEvent;
use App\Expense;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreExpensesListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreExpensesEvent  $event
     * @return void
     */
    public function handle(StoreExpensesEvent $event)
    {   //Looking up new Harvest expenses and storing them in DB
        //LOOKUP
        $harvest_expenses = collect($event->harvest_json[config('harvest.expenses')]);

        $new_expenses = $harvest_expenses
            ->whereIn('id',$harvest_expenses   //Gets Harvest expense models ..
            ->pluck('id')
                ->diff(\App\Expense::forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new expense data
        foreach($new_expenses as $new_expense) {
            $data[] = [
                'id' => $new_expense['id'],
                'employee_id' => $new_expense['user']['id'],
                'project_id' => $new_expense['project']['id'],
                'expense_category_id' => $new_expense['expense_category']['id'],
                'spent_date' => $new_expense['spent_date'],
                'total_cost' => $new_expense['total_cost'],
                'billable' => $new_expense['billable'],
                'created_at' => Carbon::parse($new_expense['created_at']),
                'updated_at' => Carbon::parse($new_expense['updated_at']),
            ];
        }
        Expense::insert($data);    //Store all new expenses
    }
}
