<?php

namespace App\Listeners\InitialStoreListeners;

use App\Events\InitialStoreEvents\StoreAllEntitiesEvent;
use App\Events\InitialStoreEvents\StoreClientsEvent;
use App\Events\InitialStoreEvents\StoreEmployeeAssignmentsEvent;
use App\Events\InitialStoreEvents\StoreEmployeesEvent;
use App\Events\InitialStoreEvents\StoreExpenseCategoriesEvent;
use App\Events\InitialStoreEvents\StoreExpensesEvent;
use App\Events\InitialStoreEvents\StoreInvoicesEvent;
use App\Events\InitialStoreEvents\StoreProjectsEvent;
use App\Events\InitialStoreEvents\StoreTaskAssignmentsEvent;
use App\Events\InitialStoreEvents\StoreTasksEvent;
use App\Events\InitialStoreEvents\StoreTimeEntriesEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class StoreAllEntitiesListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreAllEntitiesEvent  $event
     * @return void
     */
    public function handle(StoreAllEntitiesEvent $event)
    {
        dump('Queuing all entity matching jobs ...');
        $current_page = 1;
        //Employees
        DB::table('employees')->truncate();
        dump("  Table 'employees' truncated");
        do{
            $new_event = new StoreEmployeesEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in employees');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        $current_page = 1;
        //Clients
        DB::table('clients')->truncate();
        dump("  Table 'clients' truncated");
        do{
            $new_event = new StoreClientsEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in clients');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        //Projects
        DB::table('projects')->truncate();
        dump("  Table 'projects' truncated");
        $current_page = 1;
        do{
            $new_event = new StoreProjectsEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in projects');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        //Tasks
        DB::table('tasks')->truncate();
        dump("  Table 'tasks' truncated");
        $current_page = 1;
        do{
            $new_event = new StoreTasksEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in tasks');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        //Assignments
        //Employee assignments
        DB::table('employee_project')->truncate();
        dump("  Table 'employee_project' truncated");
        $current_page = 1;
        do{
            $new_event = new StoreEmployeeAssignmentsEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in employee assignments');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        //Task assignments
        DB::table('project_task')->truncate();
        dump("  Table 'project_task' truncated");
        $current_page = 1;
        do{
            $new_event = new StoreTaskAssignmentsEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in task assignments');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        //Time Entries
        DB::table('time_entries')->truncate();
        dump("  Table 'time_entries' truncated");
        $current_page = 1;
        do{
            $new_event = new StoreTimeEntriesEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in time entries');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        //Expense Categories
        DB::table('expense_categories')->truncate();
        dump("  Table 'expense_categories' truncated");
        $current_page = 1;
        do{
            $new_event = new StoreExpenseCategoriesEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in expense categories');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        //Expenses
        DB::table('expenses')->truncate();
        dump("  Table 'expenses' truncated");
        $current_page = 1;
        do{
            $new_event = new StoreExpensesEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in expenses');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        //Invoices
        DB::table('invoices')->truncate();
        DB::table('invoice_line_items')->truncate();
        dump("  Table 'invoices' truncated");
        $current_page = 1;
        do{
            $new_event = new StoreInvoicesEvent($current_page);
            event($new_event);
            dump('  Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in invoices');
            $current_page ++;
        }while($new_event->harvest_json['next_page']);
        dump('Queue complete!');
    }
}
