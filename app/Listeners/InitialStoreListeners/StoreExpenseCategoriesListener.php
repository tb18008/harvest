<?php

namespace App\Listeners\InitialStoreListeners;

use App\Events\InitialStoreEvents\StoreExpenseCategoriesEvent;
use App\ExpenseCategory;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreExpenseCategoriesListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreExpenseCategoriesEvent  $event
     * @return void
     */
    public function handle(StoreExpenseCategoriesEvent $event)
    {   //Looking up new Harvest expense categories and storing them in DB
        //LOOKUP
        $harvest_expense_categories = collect($event->harvest_json[config('harvest.expense_categories')]);

        $new_expense_categories = $harvest_expense_categories
            ->whereIn('id',$harvest_expense_categories   //Gets Harvest expense category models ..
            ->pluck('id')
                ->diff(\App\ExpenseCategory::forPage($event->harvest_json['page'],100) //.. that have different id's from the ones already in DB
                ->pluck('id')));

        //STORE
        $data = []; //Array for new expense category data
        foreach($new_expense_categories as $new_expense_category) {
            $data[] = [
                'id' => $new_expense_category['id'],
                'name' => $new_expense_category['name'],
                'created_at' => Carbon::parse($new_expense_category['created_at']),
                'updated_at' => Carbon::parse($new_expense_category['updated_at']),
            ];
        }
        ExpenseCategory::insert($data);    //Store all new expense categories
    }
}
