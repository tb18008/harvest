<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Events\RoutineUpdateEvents\UpdateTaskAssignmentsSinceEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class UpdateTaskAssignmentsSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateTaskAssignmentsSinceEvent  $event
     * @return void
     */
    public function handle(UpdateTaskAssignmentsSinceEvent $event)
    {   //Finding newly updated harvest task assignments and updating DB
        $harvest_task_assignments = collect($event->harvest_json[config('harvest.task_assignments')]);

        foreach ($harvest_task_assignments as $harvest_task_assignment){
            DB::table('project_task')->updateOrInsert(
                ['id' => $harvest_task_assignment['id']],
                [
                    'project_id' => $harvest_task_assignment['project']['id'],
                    'task_id' => $harvest_task_assignment['task']['id'],
                    'created_at' => Carbon::parse($harvest_task_assignment['created_at']),
                    'updated_at' => Carbon::parse($harvest_task_assignment['updated_at']),
                ]
            );
        }
    }
}
