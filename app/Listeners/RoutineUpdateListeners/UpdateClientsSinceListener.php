<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Client;
use App\Employee;
use App\Events\RoutineUpdateEvents\UpdateClientsSinceEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateClientsSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateClientsSinceEvent  $event
     * @return void
     */
    public function handle(UpdateClientsSinceEvent $event)
    {   //Finding newly updated harvest clients and updating DB
        $harvest_clients = collect($event->harvest_json[config('harvest.clients')]);

        foreach ($harvest_clients as $harvest_client){
            Client::updateOrCreate(
                ['id' => $harvest_client['id']],
                [
                    'name' => $harvest_client['name'],
                    'created_at' => Carbon::parse($harvest_client['created_at']),
                    'updated_at' => Carbon::parse($harvest_client['updated_at']),
                ]
            );
        }
    }
}
