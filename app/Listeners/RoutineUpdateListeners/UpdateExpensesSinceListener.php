<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Events\RoutineUpdateEvents\UpdateExpensesSinceEvent;
use App\Expense;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateExpensesSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateExpensesSinceEvent  $event
     * @return void
     */
    public function handle(UpdateExpensesSinceEvent $event)
    {   //Finding newly updated harvest expenses and updating DB
        $harvest_expenses = collect($event->harvest_json[config('harvest.expenses')]);

        foreach ($harvest_expenses as $harvest_expense){
            Expense::updateOrCreate(
                ['id' => $harvest_expense['id']],
                [
                    'employee_id' => $harvest_expense['user']['id'],
                    'project_id' => $harvest_expense['project']['id'],
                    'expense_category_id' => $harvest_expense['expense_category']['id'],
                    'spent_date' => $harvest_expense['spent_date'],
                    'total_cost' => $harvest_expense['total_cost'],
                    'billable' => $harvest_expense['billable'],
                    'created_at' => Carbon::parse($harvest_expense['created_at']),
                    'updated_at' => Carbon::parse($harvest_expense['updated_at']),
                ]
            );
        }
    }
}
