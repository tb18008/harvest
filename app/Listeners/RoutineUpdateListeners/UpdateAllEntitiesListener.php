<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Events\RoutineUpdateEvents\UpdateAllEntitiesEvent;
use App\Events\RoutineUpdateEvents\UpdateClientsSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateEmployeeAssignmentsSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateEmployeesSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateExpenseCategoriesSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateExpensesSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateInvoicesSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateProjectsSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateTaskAssignmentsSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateTasksSinceEvent;
use App\Events\RoutineUpdateEvents\UpdateTimeEntriesSinceEvent;
use App\HarvestService;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateAllEntitiesListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateAllEntitiesEvent  $event
     * @return void
     */
    public function handle(UpdateAllEntitiesEvent $event)
    {
        $harvest = new HarvestService();
        dump('Queuing entity update jobs since '.Carbon::parse($harvest->getLastUpdated(). ' ...'));
        $current_page = 1;
        //Employees
        do{
            $new_event = new UpdateEmployeesSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.employees')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in employees');
        }while($new_event->harvest_json['next_page']);
        $current_page = 1;
        //Clients
        do{
            $new_event = new UpdateClientsSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.clients')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in clients');
        }while($new_event->harvest_json['next_page']);
        //Projects
        $current_page = 1;
        do{
            $new_event = new UpdateProjectsSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.projects')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in projects');
        }while($new_event->harvest_json['next_page']);
        //Tasks
        $current_page = 1;
        do{
            $new_event = new UpdateTasksSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.tasks')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in tasks');
        }while($new_event->harvest_json['next_page']);
        //Assignments
        //Employee assignments
        $current_page = 1;
        do{
            $new_event = new UpdateEmployeeAssignmentsSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.employee_assignments')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in employee assignments');
        }while($new_event->harvest_json['next_page']);
        //Task assignments
        $current_page = 1;
        do{
            $new_event = new UpdateTaskAssignmentsSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.task_assignments')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in task assignments');
        }while($new_event->harvest_json['next_page']);
        //Time Entries
        $current_page = 1;
        do{
            $new_event = new UpdateTimeEntriesSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.time_entries')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in time entries');
        }while($new_event->harvest_json['next_page']);
        //Expense Categories
        $current_page = 1;
        do{
            $new_event = new UpdateExpenseCategoriesSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.expense_categories')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in expense categories');
        }while($new_event->harvest_json['next_page']);
        //Expenses
        $current_page = 1;
        do{
            $new_event = new UpdateExpensesSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.expenses')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in expenses');
        }while($new_event->harvest_json['next_page']);
        //Invoices
        $current_page = 1;
        do{
            $new_event = new UpdateInvoicesSinceEvent($current_page);
            $current_page ++;
            if(sizeof($new_event->harvest_json[config('harvest.invoices')])){
                event($new_event);    //Some items need to be updated
            }
            dump('Page '.$new_event->harvest_json['page'].' of '.$new_event->harvest_json['total_pages']. ' in invoices');
        }while($new_event->harvest_json['next_page']);
        dump('Queue complete!');
    }
}
