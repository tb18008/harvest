<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Employee;
use App\Events\RoutineUpdateEvents\UpdateEmployeesSinceEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateEmployeesSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateEmployeesSinceEvent  $event
     * @return void
     */
    public function handle(UpdateEmployeesSinceEvent $event)
    {   //Finding newly updated harvest employees and updating DB
        $harvest_employees = collect($event->harvest_json[config('harvest.employees')]);

        foreach ($harvest_employees as $harvest_employee){
            Employee::updateOrCreate(
                ['id' => $harvest_employee['id']],
                [
                    'first_name' => $harvest_employee['first_name'],
                    'last_name' => $harvest_employee['last_name'],
                    'email' => $harvest_employee['email'],
                    'cost_rate' => $harvest_employee['cost_rate'],
                    'created_at' => Carbon::parse($harvest_employee['created_at']),
                    'updated_at' => Carbon::parse($harvest_employee['updated_at']),
                ]
            );
        }
    }
}
