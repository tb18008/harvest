<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\BudgetType;
use App\Events\RoutineUpdateEvents\UpdateProjectsSinceEvent;
use App\HourlyRateType;
use App\Project;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateProjectsSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateProjectsSinceEvent  $event
     * @return void
     */
    public function handle(UpdateProjectsSinceEvent $event)
    {   //Finding newly updated harvest projects and updating DB
        $harvest_projects = collect($event->harvest_json[config('harvest.projects')]);

        foreach ($harvest_projects as $harvest_project){
            Project::updateOrCreate(
                ['id' => $harvest_project['id']],
                [
                    'client_id' => $harvest_project['client']['id'],
                    'name' => $harvest_project['name'],
                    'budget' => $harvest_project['budget'],
                    'budget_type_id' => BudgetType::where('type', $harvest_project['budget_by'])->first()->id,
                    'hourly_rate' => $harvest_project['hourly_rate'],
                    'hourly_rate_type_id' => HourlyRateType::where('type',$harvest_project['bill_by'])->first()->id,
                    'is_active' => $harvest_project['is_active'],
                    'is_billable' => $harvest_project['is_billable'],
                    'is_fixed_fee' => $harvest_project['is_fixed_fee'],
                    'created_at' => Carbon::parse($harvest_project['created_at']),
                    'updated_at' => Carbon::parse($harvest_project['updated_at']),
                ]
            );
        }
    }
}
