<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Events\RoutineUpdateEvents\UpdateEmployeeAssignmentsSinceEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class UpdateEmployeeAssignmentsSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateEmployeeAssignmentsSinceEvent  $event
     * @return void
     */
    public function handle(UpdateEmployeeAssignmentsSinceEvent $event)
    {   //Finding newly updated harvest employee assignments and updating DB
        $harvest_employee_assignments = collect($event->harvest_json[config('harvest.employee_assignments')]);

        foreach ($harvest_employee_assignments as $harvest_employee_assignment){
            DB::table('employee_project')->updateOrInsert(
                ['id' => $harvest_employee_assignment['id']],
                [
                    'project_id' => $harvest_employee_assignment['project']['id'],
                    'employee_id' => $harvest_employee_assignment['user']['id'],
                    'created_at' => Carbon::parse($harvest_employee_assignment['created_at']),
                    'updated_at' => Carbon::parse($harvest_employee_assignment['updated_at']),
                ]
            );
        }
    }
}
