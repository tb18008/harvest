<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Events\RoutineUpdateEvents\UpdateTasksSinceEvent;
use App\Task;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateTasksSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateTasksSinceEvent  $event
     * @return void
     */
    public function handle(UpdateTasksSinceEvent $event)
    {   //Finding newly updated harvest clients and updating DB
        $harvest_tasks = collect($event->harvest_json[config('harvest.tasks')]);

        foreach ($harvest_tasks as $harvest_task){
            Task::updateOrCreate(
                ['id' => $harvest_task['id']],
                [
                    'name' => $harvest_task['name'],
                    'created_at' => Carbon::parse($harvest_task['created_at']),
                    'updated_at' => Carbon::parse($harvest_task['updated_at']),
                ]
            );
        }
    }
}
