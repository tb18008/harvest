<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Events\RoutineUpdateEvents\UpdateInvoicesSinceEvent;
use App\Invoice;
use App\InvoiceLineItem;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateInvoicesSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateInvoicesSinceEvent  $event
     * @return void
     */
    public function handle(UpdateInvoicesSinceEvent $event)
    {   //Finding newly updated harvest invoices and updating DB
        $harvest_invoices = collect($event->harvest_json[config('harvest.invoices')]);

        foreach ($harvest_invoices as $harvest_invoice){
            Invoice::updateOrCreate(
                ['id' => $harvest_invoice['id']],
                [
                    'client_id' => $harvest_invoice['client']['id'],
                    'amount' => $harvest_invoice['amount'],
                    'issue_date' => $harvest_invoice['issue_date'],
                    'created_at' => Carbon::parse($harvest_invoice['created_at']),
                    'updated_at' => Carbon::parse($harvest_invoice['updated_at']),
                ]
            );
            foreach($harvest_invoice['line_items'] as $line_item){
                if($line_item['project']) $line_item['project'] = $line_item['project']['id'];
                InvoiceLineItem::updateOrCreate(
                    ['id' => $line_item['id']],
                    [
                        'invoice_id' => $harvest_invoice['id'],
                        'project_id' => $line_item['project'],
                        'amount' => $line_item['amount'],
                    ]
                );
            }
        }
    }
}
