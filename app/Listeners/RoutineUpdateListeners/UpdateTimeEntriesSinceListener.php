<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Events\RoutineUpdateEvents\UpdateTimeEntriesSinceEvent;
use App\TimeEntry;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateTimeEntriesSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateTimeEntriesSinceEvent  $event
     * @return void
     */
    public function handle(UpdateTimeEntriesSinceEvent $event)
    {   //Finding newly updated harvest time entries and updating DB
        $harvest_time_entries = collect($event->harvest_json[config('harvest.time_entries')]);

        foreach ($harvest_time_entries as $harvest_time_entry){
            TimeEntry::updateOrCreate(
                ['id' => $harvest_time_entry['id']],
                [
                    'employee_id' => $harvest_time_entry['user']['id'],
                    'project_id' => $harvest_time_entry['project']['id'],
                    'hours' => $harvest_time_entry['hours'],
                    'spent_date' => $harvest_time_entry['spent_date'],
                    'task_id' => $harvest_time_entry['task']['id'],
                    'created_at' => Carbon::parse($harvest_time_entry['created_at']),
                    'updated_at' => Carbon::parse($harvest_time_entry['updated_at']),
                ]
            );
        }
    }
}
