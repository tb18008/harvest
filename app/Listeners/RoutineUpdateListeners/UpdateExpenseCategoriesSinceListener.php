<?php

namespace App\Listeners\RoutineUpdateListeners;

use App\Events\RoutineUpdateEvents\UpdateExpenseCategoriesSinceEvent;
use App\ExpenseCategory;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateExpenseCategoriesSinceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateExpenseCategoriesSinceEvent  $event
     * @return void
     */
    public function handle(UpdateExpenseCategoriesSinceEvent $event)
    {   //Finding newly updated harvest expense categories and updating DB
        $harvest_expense_categories = collect($event->harvest_json[config('harvest.expense_categories')]);

        foreach ($harvest_expense_categories as $harvest_expense_category){
            ExpenseCategory::updateOrCreate(
                ['id' => $harvest_expense_category['id']],
                [
                    'name' => $harvest_expense_category['name'],
                    'created_at' => Carbon::parse($harvest_expense_category['created_at']),
                    'updated_at' => Carbon::parse($harvest_expense_category['updated_at']),
                ]
            );
        }
    }
}
