<?php

namespace App\Listeners\EmployeeNotificationListeners;

use App\Employee;
use App\Events\EmployeeNotificationEvents\CheckUnfilledTimesheetsEvent;
use App\TimeEntry;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CheckUnfilledTimesheetsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckUnfilledTimesheetsEvent  $event
     * @return void
     */
    public function handle(CheckUnfilledTimesheetsEvent $event)
    {
        if(!Carbon::now()->isWeekday())
         return;

        $all_employees = Employee::all();

        $date = Carbon::now()->startOfDay();

        $employees_with_entries = TimeEntry::where('spent_date',$date)
            ->get()
            ->unique('employee_id')
            ->pluck('employee_id');

        $all_employees->find($all_employees
            ->pluck('id')
            ->diff($employees_with_entries)
        )->each(function ($employee) use ($date){
            $employee->notify(new \App\Notifications\UnfilledTimesheet($employee, $date));
        });
    }
}
