<?php

namespace App\Listeners;

use App\Events\RemoveDuplicateJobsEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class RemoveDuplicateJobsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RemoveDuplicateJobsEvent  $event
     * @return void
     */
    public function handle(RemoveDuplicateJobsEvent $event)
    {
        //SELECTING JOBS THAT ARE NOT UNIQUE

        $all_payloads = DB::table('jobs')
            ->get()
            ->pluck('payload')
            ->map(function($item){
                return json_decode($item, true); //Converts json data strings to arrays
            });

        $duplicate_payloads = $all_payloads
            ->whereIn('uuid', $all_payloads //Select payloads ..
            ->pluck('uuid')
                ->diff($all_payloads
                    ->unique('displayName') //.. that don't have unique displayName
                    ->pluck('uuid')
                )
            )
            ->map(function($item){  //Converts arrays to json data strings
                return collect($item)->toJson();
            });

        //DELETING DUPLICATE JOBS
        DB::table('jobs')->whereIn('payload',$duplicate_payloads)->delete();
    }
}
