<?php

namespace App;
use App\Jobs\LogRequestJob;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;

class ImportDefaultsService
{
    private $defaults;

    public function __construct()
    {
        $this->defaults = ImportDefault::all();
    }

    public function assignDefaultValues($statement_entries){

        return collect($statement_entries)->transform(function ($statement_entry){
            $statement_entry_defaults = $this->defaults
                ->where('client_name', $statement_entry->client_name)
                ->first();
            if($statement_entry_defaults){
                $statement_entry->update(collect($statement_entry_defaults)
                    ->except('id')
                    ->toArray());
                return $statement_entry;
            }
            return $statement_entry;
        });
    }

    public function storeDefaultValues($statement_entries){

        foreach ($statement_entries as $statement_entry){
            $client_name = collect($statement_entry)->pull('client_name');
            ImportDefault::updateOrCreate(
                ['client_name' => $client_name],
                $statement_entry
            );
        }

    }
}
