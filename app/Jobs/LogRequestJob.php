<?php

namespace App\Jobs;

use App\RequestLoggingService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LogRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $request_logging_service;

    private $is_incoming;

    /**
     * Create a new job instance.
     *
     * @param $request_data
     * @param $is_incoming
     */

    public function __construct($request_data, $is_incoming)
    {
        $this->request_logging_service = new RequestLoggingService($request_data);
        $this->is_incoming = $is_incoming;
    }

    /**
     * Execute the job.
     *
     * @param $request_data
     * @return void
     */
    public function handle()
    {
        if($this->is_incoming) $this->request_logging_service->logIncoming();
        else $this->request_logging_service->logOutgoing();
    }
}
