<?php

namespace App;
use App\Jobs\LogRequestJob;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Http;

class HarvestService
{
    private $url_head, $url_tail; //HTTP request URL parts

    public $message;    //Message for redirects

    public function __construct()
    {
        $this->url_head = config('harvest.request_head');
        $this->url_tail = config('harvest.request_tail');
    }

    public function getObjects($object_type, $page){   //Universal for all Harvest entities (objects)

        $url = $this->url_head.config('harvest.'.$object_type).'?'.'page='.$page.$this->url_tail;

        $response = Http::get($url);

        $this->log($url,null, $response,'GET');

        return $response->json();
    }

    public function getUpdatedSince($object_type, $page){

        $url = $this->url_head.
            config('harvest.'.$object_type).'?'.
            'updated_since='.Carbon::parse($this->getLastUpdated())->toIso8601ZuluString().'&'.
            'page='.$page.
            $this->url_tail;

        $response = Http::get($url);

        $this->log($url,null, $response,'GET');

        return $response->json();
    }

    public function getLastUpdated(){
        $max = collect([Employee::max('updated_at'),Client::max('updated_at'),Project::max('updated_at'),TimeEntry::max('updated_at')])->max();
        if($max) return $max;
        return null;
    }

    public function createObject($object_type, $request_data){

        $url = $this->url_head.config('harvest.'.$object_type).'?'.$this->url_tail;

        $response = Http::post($url,$request_data);

        $this->log($url,$request_data, $response,'POST');

        if($response->failed()){ //The outgoing request was invalid
            $this->getMessage(
                false,
                'create',
                $object_type,
                null,
                $response->toPsrResponse()->getReasonPhrase()
            );
            return null;
        }


        //The outgoing request was valid
        $model_name = config('model_names.'.$object_type.'_full');
        $new_model = $model_name::create($this->toFillable($response));

        $this->getMessage(
            true,
            'create',
            $object_type,
            $new_model,
            null
        );
        return $new_model;
    }

    public function updateObject($object_type, $object_id ,$request_data){

        $url = $this->url_head.config('harvest.'.$object_type).'/'.$object_id.'?'.$this->url_tail;

        $response = Http::patch($url,$request_data);

        $this->log($url,$request_data, $response,'PATCH');

        if($response->failed()){ //The outgoing request was invalid
            $this->getMessage(
                false,
                'update',
                $object_type,
                null,
                $response->toPsrResponse()->getReasonPhrase()
            );
            return null;
        }

        //The outgoing request was valid
        $model_name = config('model_names.'.$object_type.'_full');
        $updated_model = $model_name::find($object_id);
        $updated_model->update($this->toFillable($response));

        $this->getMessage(
            true,
            'update',
            $object_type,
            $updated_model,
            null
        );
        return $updated_model;
    }

    public function deleteObject($object_type, $object_id){

        $url = $this->url_head.config('harvest.'.$object_type).'/'.$object_id.'?'.$this->url_tail;

        $response = Http::delete($url);

        $this->log($url,null, $response,'DELETE');

        if($response->failed()){    //The outgoing request was invalid
            $this->getMessage(
                false,
                'delete',
                $object_type,
                null,
                $response->toPsrResponse()->getReasonPhrase()
            );
            return null;
        }

        //The outgoing request was valid
        $model_name = config('model_names.'.$object_type.'_full');
        $deleted_model = $model_name::find($object_id);
        $deleted_model->delete();

        $this->getMessage(
            true,
            'delete',
            $object_type,
            $deleted_model,
            null
        );
        return $deleted_model;
    }

    public function getObjectAssignments($object_type, $object_id, $assign_type){

        $url = $this->url_head.config('harvest.'.$object_type).'/'.$object_id.'/'.$assign_type.'?'.$this->url_tail;

        $response = Http::get($url);

        $this->log($url,null, $response,'GET');

        return $response->json();
    }

    public function createAssignment($object_type, $object_id, $assign_type, $request_data){

        $url = $this->url_head.
            config('harvest.'.$object_type).'/'.
            $object_id.'/'.
            config('harvest.'.$assign_type).'?'.
            $this->url_tail;

        $response = Http::post($url,$request_data);

        $this->log($url,$request_data, $response,'POST');

        return $response->json();
    }

    public function deleteAssignment($object_type, $object_id, $assign_type, $assign_id){

        $url = $this->url_head.
            config('harvest.'.$object_type).'/'.
            $object_id.'/'.
            config('harvest.'.$assign_type).'/'.
            $assign_id.'?'.
            $this->url_tail;

        $response = Http::delete($url);

        $this->log($url,null, $response,'DELETE');

        return $response->json();
    }

    public function getMessage($was_successful, $method, $object_type, $object, $error = null){
        if($was_successful){
            $this->message = [
                'success'=> __(
                    'messages.'.$method.'_success',
                    [
                        'object_type'=>config('model_names.'.$object_type.'_formatted'),
                        'object'=>$object->name,
                    ]
                )
            ];
            return;
        }
        $this->message = [
            'fail' => __(
                'messages.'.$method.'_fail',
                [
                    'object_type'=>config('model_names.'.$object_type.'_formatted'),
                    'error'=>$error,
                ]
            )
        ];
    }

    private function log($url, $request, $response, $method){

        if($response->failed()){
            $request_data = [
                'url' => $url,
                'request' => collect($request)->toJson(),
                'response' => collect($response->json())->toJson(),
                'method' => $method,
                'status' => 'fail',
                'code' => $response->status(),
                'reason_phrase' => $response->toPsrResponse()->getReasonPhrase(),
                'created_at' =>  date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            LogRequestJob::dispatchNow($request_data, false);

            return redirect()->back()->with('message',$response);
        }

        $request_data = [
            'url' => $url,
            'request' => collect($request)->toJson(),
            'response' => collect($response->json())->toJson(),
            'method' => $method,
            'status' => 'success',
            'code' => $response->status(),
            'reason_phrase' => $response->toPsrResponse()->getReasonPhrase(),
            'created_at' =>  date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        LogRequestJob::dispatchNow($request_data, false);

        return null;
    }

    private function toFillable($response_json){
        $replacables = ['client','project','user','task','expense_category','role', 'user_assignment'];
        $array = collect($response_json->json());

        foreach ($replacables as $replacable){
            if($array->has($replacable)){
                $array->put(config('harvest.response_'.$replacable).'_id',$array[$replacable]['id']);
                $array->pull($replacable);
            }
        }

        if($array->has('budget_by')){
            $array
                ->put(
                    'budget_type_id',
                    BudgetType::where('type', $array['budget_by'])
                        ->first()
                        ->id
                );
            $array
                ->put(
                    'hourly_rate_type_id',
                    HourlyRateType::where('type',$array['bill_by'])
                        ->first()
                        ->id
                );
        }

        return $array->toArray();
    }
}
