<?php


namespace App;


use Carbon\Carbon;

class StatementService
{
    public function import($user_id, $csv_file){
        $statement = new Statement;
        $statement->user_id = $user_id;
        $statement->save();

        $statement_entries = $this->format_csv($csv_file, $statement->id);
        foreach ($statement_entries as $statement_entry) StatementEntry::create($statement_entry);

        return $statement->id;
    }

    private function format_csv($csv_file, $statement_id)
    {
        return collect($this->csv_to_array($csv_file)['rows'])
            ->filter(function ($value) {
                return $value[1] == "20" && $value[7] == "D";   //Filters out account summary and deposits
            })->map(function($value) use ($statement_id){
                $value[2] = Carbon::createFromFormat("d.m.Y",$value[2])->toDateString();    //Formatting date
                $value[4] = preg_replace('/\s+/', ' ', $value[4]);  //Removing multiple spaces from description
                $value[5] = str_replace(",",".",$value[5]); //Formatting decimal separator
                return [
                    'statement_id' => $statement_id,
                    'spent_date' => $value[2],
                    'client_name' => $value[3],
                    'description' => $value[4],
                    'total_cost' => $value[5],
                ];
            });
    }

    private function csv_to_array($filename='', $delimiter=';')
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $column_names = "";
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                array_pop($row);
                if(!$header){
                    $column_names = $row;
                    $header = $row;
                }
                else
                    $data[] = $row;
            }
            fclose($handle);
        }
        return array_merge(
            [
                'header'=>$column_names,
            ],
            [
                'rows'=>$data
            ]
        );
    }
}
