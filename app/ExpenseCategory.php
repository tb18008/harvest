<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenseCategory extends Model
{
    protected $fillable = ['id','name'];

    public function expenses()
    {
        return $this->hasMany('App\Expense');
    }
}
