<?php


namespace App;

use Carbon\Carbon;

class ReportService
{
    public function getPeriod($time_type, $prev_next = null, $start_date = null, $end_date = null){
        if($time_type == 'custom'){
            return [    //Custom period, uses $start_date and $end_date
                'start_date' => Carbon::parse($start_date),
                'end_date' => Carbon::parse($end_date),
            ];
        }

        if($time_type == 'all') return null;
        else{
            if($start_date){  //Start date passed => previous or next time type
                if($prev_next == 'prev') return $this->getPreviousPeriod($time_type, $start_date, $end_date);
                return $this->getNextPeriod($time_type, $start_date, $end_date);
            }
            return config('periods.'.$time_type);  //No given start date => time type of now
        }
    }
    public function getPreviousPeriod($time_type, $start_date, $end_date){
        if($time_type!='semimonth'){
            return [
                'start_date' => Carbon::parse($start_date)->sub($time_type,1),
                'end_date' => Carbon::parse($end_date)->sub($time_type,1),
            ];
        }
        return [
            'start_date' => Carbon::parse($start_date)->sub('week',2),
            'end_date' => Carbon::parse($end_date)->sub('week',2),
        ];
    }
    public function getNextPeriod($time_type, $start_date, $end_date){
        if($time_type!='semimonth'){
            return [
                'start_date' => Carbon::parse($start_date)->add($time_type,1),
                'end_date' => Carbon::parse($end_date)->add($time_type,1),
            ];
        }
        return [
            'start_date' => Carbon::parse($start_date)->add('week',2),
            'end_date' => Carbon::parse($end_date)->add('week',2),
        ];
    }
    public function getTimetable($time_type, $prev_next = null, $start_date = null, $end_date = null){
        //Narrow time entries to period of time
        $period = $this->getPeriod($time_type, $prev_next, $start_date, $end_date);
        $start_date = $period['start_date'];
        $end_date = $period['end_date'];
        if($period) $time_entries = TimeEntry::whereBetween('spent_date', [$period['start_date'],$period['end_date']])
            ->with(['employee','project'])
            ->get();
        else $time_entries = TimeEntry::with(['employee','project'])->get(); //getPeriod return null, all entries

        $items = $time_entries  //Variable to be returned as the value for the 'items' key
        ->groupBy('project.id')
            ->map(function ($project_time_entries, $key){   //Time entries grouped by projects (key = project id)

                $grouped_by_employee_id = $project_time_entries->groupBy('employee_id');

                //Project specific variables
                $time_entry_project = $project_time_entries->first()->project;
                $budget = $time_entry_project->budget;

                $project = collect([
                    'project_id' => $key,
                    'project_name' => $time_entry_project->name,
                    'budget' => $budget,
                    'spent' => TimeEntry::where('project_id', $key)->sum('hours'),
                ]);

                if(!$budget || $budget == 0){
                    //(Project doesn't have  a budget)
                    $hours = $grouped_by_employee_id
                        ->map(function ($employee_time_entries, $key){
                            return [
                                'employee_id' => $key,
                                'paid' => 0,
                                'unpaid' => $employee_time_entries->sum('hours'),   //All hours are unpaid
                            ];
                        });
                    return $project->merge([
                        'hours' => $hours,
                        'paid' => $hours->sum(function ($employee){
                            return $employee['paid'];
                        }),
                        'unpaid' => $hours->sum(function ($employee){
                            return $employee['unpaid'];
                        }),
                    ]);
                }

                //(Project has a budget)

                if($project_time_entries->sum('hours')<=$budget){
                    //(All hours are within budget)
                    $hours = $grouped_by_employee_id
                        ->map(function ($employee_time_entries, $key){
                            return [
                                'employee_id' => $key,
                                'paid' => $employee_time_entries->sum('hours'),  //All hours are paid
                                'unpaid' => 0
                            ];
                        });
                    return $project->merge([
                        'hours' => $hours,
                        'paid' => $hours->sum(function ($employee){
                            return $employee['paid'];
                        }),
                        'unpaid' => $hours->sum(function ($employee){
                            return $employee['unpaid'];
                        }),
                    ]);
                }

                //(Not all hours are withing budget)
                $merged = null; //Result collection used in further merge() operations

                //Partitioning into two parts: withing budget / over budget. Summing up the spent hours on project
                $spent = 0;
                list($within_budget, $over_budget) = $project_time_entries->partition(function ($time_entry) use (&$spent, $budget) {
                    $spent += $time_entry->hours;
                    return $spent <= $budget;
                });

                // +       within budget (all paid)
                //+/-      shift (one partially paid, partially unpaid or one fully unpaid)
                // -       over budget (all unpaid)

                //WITHIN BUDGET
                if($within_budget->isNotEmpty()){   //The first time entry was within budget (starts with within budget)
                    $within_budget_sum = $within_budget->sum('hours');  //Hours without the 'shift' time entry. Used to subtract from budget and split shift time entry hours

                    $within_budget = $within_budget->groupBy('employee_id')->map(function ($employee_time_entries, $key){
                        return [
                            'employee_id' => $key,
                            'paid' => $employee_time_entries->sum('hours'), //Filling the sum of employee's hours as paid because they are within budget
                            'unpaid' => 0,
                        ];
                    });

                    $merged = $within_budget;   //Setting first collection which will be merged with other collections
                }

                //SHIFT
                $shift = $over_budget->shift(); //Cutting out the first time entry that is over budget

                //Formatting models into arrays of paid and unpaid

                $shift = collect([
                    $shift->employee_id => [
                        'employee_id' => $shift->employee_id,
                        'paid' => $budget - $within_budget_sum,
                        //If $within_budget_sum == $budget, then $shift is just like an over budget time entry
                        'unpaid' => $shift->hours - ($budget - $within_budget_sum),
                        //If $within_budget_sum == $budget, all hours are unpaid
                    ]
                ]);

                //Merging withing budget with shift

                $merged = $merged->merge($shift);

                //OVER BUDGET
                if($over_budget->isNotEmpty()) {
                    $over_budget = $over_budget->groupBy('employee_id')->map(function ($employee_time_entries, $key){
                        return [
                            'employee_id' => $key,
                            'paid' => 0,
                            'unpaid' => $employee_time_entries->sum('hours'),
                        ];
                    });
                    $merged = $merged->merge($over_budget);
                }

                $hours = $merged->groupBy('employee_id')
                    ->map(function ($employee_time_entries, $key){
                        return [
                            'employee_id' => $key,
                            'paid' => $employee_time_entries->sum('paid'),
                            'unpaid' => $employee_time_entries->sum('unpaid'),
                        ];
                    });

                //RETURN
                return $project->merge([
                    'hours' => $hours,
                    'paid' => $hours->sum(function ($employee){
                        return $employee['paid'];
                    }),
                    'unpaid' => $hours->sum(function ($employee){
                        return $employee['unpaid'];
                    }),
                ]);
            });

        //Calculating totals

        $employees = $time_entries  //Employee models and their paid and unpaid totals
        ->unique('employee_id')
            ->map(function($time_entry) use ($items){
                $employee_id = $time_entry->employee->id;
                return [
                    'model' => $time_entry->employee,
                    'paid' => $items->sum(function ($project) use ($employee_id){
                        return collect($project['hours'])->get($employee_id)['paid'];
                    }),
                    'unpaid' => $items->sum(function ($project) use ($employee_id){
                        return collect($project['hours'])->get($employee_id)['unpaid'];
                    })
                ];
            })
            ->sortby(function($employee){
                return $employee['model']->first_name. ' '. $employee['model']->last_name;
            });

        $total = [
            'paid' => $employees->sum(function ($employee) {
                return $employee['paid'];
            }),
            'unpaid' => $employees->sum(function ($employee) {
                return $employee['unpaid'];
            }),
        ];

        return [
            'report_type' => 'timetable',
            'time_type' => $time_type,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'items' => $items->sortBy('project_name'),
            'employees' => $employees, //Unique employees in time entries sorted by their full name
            'total' => $total,
        ];
    }
    public function getNetIncome($time_type, $prev_next = null, $start_date = null, $end_date = null){
        $period = $this->getPeriod($time_type, $prev_next, $start_date, $end_date);
        $start_date = $period['start_date'];
        $end_date = $period['end_date'];

        $clients = Client::with(
            'projects',
            'projects.time_entries',
            'projects.expenses',
            'projects.time_entries.employee',
            'invoices'
        )
            ->get()
            ->filter(function ($client){
                if($client->projects->isEmpty()) return false; //Client doesn't have projects
                if(!$client->projects->first()->time_entries->isEmpty() && !$client->projects->first()->expenses->isEmpty() && $client->invoices->isEmpty()) return false;
                return true;
            }); //Excludes clients that have no amounts in report

        $items = $clients->map(function ($client) use ($start_date, $end_date){
            $projects = $client->projects->map(function ($project) use ($start_date, $end_date){
                return [
                    'project' => $project,
                    'time_entries' => $project->time_entries
                        ->sum(function ($time_entry) use ($start_date, $end_date){
                            if(Carbon::parse($time_entry->spent_date)
                                ->isBetween($start_date,$end_date)  //Time spent in selected period
                            ) return $time_entry->employee->cost_rate * $time_entry->hours;
                            return 0;
                        }),
                    'expenses' => $project->expenses->sum(function ($expense) use ($start_date, $end_date){
                        if($expense->billable &&                //Expense is billable
                            Carbon::parse($expense->spent_date)
                                ->isBetween($start_date,$end_date)  //Expense spent in selected period
                        ) return $expense->total_cost;
                        return 0;
                    }),
                    'invoiced' => $project->invoice_line_items->sum(function ($invoice_line_item) use ($start_date, $end_date){
                        if($invoice_line_item->invoice->period_start){  //Invoice has a period
                            if(
                                Carbon::parse($invoice_line_item->invoice->period_start)
                                    ->isAfter(Carbon::parse($start_date)->sub(1,'days')) &&
                                Carbon::parse($invoice_line_item->invoice->period_end)
                                    ->isBefore(Carbon::parse($end_date)->add(1,'days'))
                            ) return $invoice_line_item->amount;    //Invoice period start and end is between selected period
                            return 0;
                        }
                        //Invoice doesn't have a period
                        if(
                        Carbon::parse($invoice_line_item->invoice->issue_date)
                            ->between($start_date, $end_date)
                        ) return $invoice_line_item->amount;
                        return 0;
                    }),
                ];
            })->reject(function ($project) {
                if(
                    !$project['invoiced'] &&
                    !$project['time_entries'] &&
                    !$project['expenses']
                )
                    return true;  //All project sums are zeros
                return false;
            })->sortBy(function ($project){
                return $project['project']->name;
            });
            return [
                'client' => $client,
                'projects' => $projects->toArray(),
                'totals' => [
                    'invoiced'=>$projects->sum('invoiced'),
                    'time_entries'=>$projects->sum('time_entries'),
                    'expenses'=>$projects->sum('expenses'),
                ],
            ];
        })->reject(function ($client) {
            if(collect($client['projects'])->isEmpty())
                return true;  //Client doesn't have project with information
            return false;
        })->sortBy(function ($client){
            return $client['client']->name;
        });

        $totals = collect([
            'invoiced' => collect($items)->sum(function ($client){
                return collect($client['projects'])->sum(function ($project){
                    return $project['invoiced'];
                });
            }),
            'time_entries' => collect($items)->sum(function ($client){
                return collect($client['projects'])->sum(function ($project){
                    return $project['time_entries'];
                });
            }),
            'expenses' => collect($items)->sum(function ($client){
                return collect($client['projects'])->sum(function ($project){
                    return $project['expenses'];
                });
            }),
        ]);

        return [
            'report_type' => 'net_income',
            'time_type' => $time_type,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'items' => $items,
            'totals' => $totals,
        ];
    }
}
