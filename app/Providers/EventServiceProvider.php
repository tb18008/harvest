<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \App\Events\InitialStoreEvents\StoreAllEntitiesEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreAllEntitiesListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreEmployeesEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreEmployeesListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreClientsEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreClientsListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreProjectsEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreProjectsListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreTasksEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreTasksListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreTaskAssignmentsEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreTaskAssignmentsListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreEmployeeAssignmentsEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreEmployeeAssignmentsListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreTimeEntriesEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreTimeEntriesListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreExpenseCategoriesEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreExpenseCategoriesListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreExpensesEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreExpensesListener::class,
        ],
        \App\Events\InitialStoreEvents\StoreInvoicesEvent::class => [
            \App\Listeners\InitialStoreListeners\StoreInvoicesListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateAllEntitiesEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateAllEntitiesListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateEmployeesSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateEmployeesSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateClientsSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateClientsSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateProjectsSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateProjectsSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateTasksSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateTasksSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateTaskAssignmentsSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateTaskAssignmentsSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateEmployeeAssignmentsSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateEmployeeAssignmentsSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateTimeEntriesSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateTimeEntriesSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateExpenseCategoriesSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateExpenseCategoriesSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateExpensesSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateExpensesSinceListener::class,
        ],
        \App\Events\RoutineUpdateEvents\UpdateInvoicesSinceEvent::class => [
            \App\Listeners\RoutineUpdateListeners\UpdateInvoicesSinceListener::class,
        ],
        \App\Events\RemoveDuplicateJobsEvent::class => [
            \App\Listeners\RemoveDuplicateJobsListener::class,
        ],
        \App\Events\EmployeeNotificationEvents\CheckUnfilledTimesheetsEvent::class => [
            \App\Listeners\EmployeeNotificationListeners\CheckUnfilledTimesheetsListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
