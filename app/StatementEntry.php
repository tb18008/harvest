<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatementEntry extends Model
{
    protected $fillable = ['statement_id', 'spent_date', 'client_name', 'description', 'total_cost', 'include','project_id','user_id','expense_category_id','billable'];

    public function statement()
    {
        return $this->belongsTo('App\Statement');
    }
}
