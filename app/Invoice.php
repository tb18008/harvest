<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['id', 'client_id','amount','issue_date'];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function invoice_line_items()
    {
        return $this->hasMany('App\InvoiceLineItem');
    }

    public function getNameAttribute()
    {
        return $this->id;
    }
}
