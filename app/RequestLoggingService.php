<?php

namespace App;

use Illuminate\Support\Facades\DB;

class RequestLoggingService
{
    private $request_data;

    public function __construct($request_data)
    {
        $this->request_data = $request_data;
    }

    public function logIncoming(){
        DB::table('incoming_requests')->insert($this->request_data);
    }

    public function logOutgoing(){
        DB::table('outgoing_requests')->insert($this->request_data);
    }
}
