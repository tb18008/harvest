<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportDefaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_defaults', function (Blueprint $table) {
            $table->id();
            $table->string('client_name')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('project_id')->nullable();
            $table->foreignId('expense_category_id')->nullable();
            $table->boolean('billable')->nullable();
            $table->boolean('include');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_defaults');
    }
}
