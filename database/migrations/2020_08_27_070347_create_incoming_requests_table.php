<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incoming_requests', function (Blueprint $table) {
            $table->id();
            $table->string('url',2000);
            $table->text('request');
            $table->string('method');
            $table->string('status');
            $table->boolean('authorized');
            $table->foreignId('user_id')->nullable();
            $table->text('errors')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_requests');
    }
}
