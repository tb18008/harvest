<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRequiredFieldsToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->foreignId('client_id');
            $table->boolean('is_active');
            $table->boolean('is_billable');
            $table->boolean('is_fixed_fee');
            $table->foreignId('hourly_rate_type_id');
            $table->foreignId('budget_type_id');
            $table->decimal('fee', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('client_id');
            $table->dropColumn('is_billable');
            $table->dropColumn('is_fixed_fee');
            $table->dropColumn('hourly_rate_type_id');
            $table->dropColumn('budget_type_id');
            $table->dropColumn('fee');
        });
    }
}
