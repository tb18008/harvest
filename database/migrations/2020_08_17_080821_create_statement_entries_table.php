<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatementEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statement_entries', function (Blueprint $table) {
            $table->id();
            $table->foreignId('statement_id');
            $table->timestamp('spent_date');
            $table->string('client_name')->nullable();
            $table->string('description');
            $table->decimal('total_cost',8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statement_entries');
    }
}
