<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultFieldsToStatementEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statement_entries', function (Blueprint $table) {
            $table->boolean('include')->default(true);
            $table->foreignId('user_id')->nullable();
            $table->foreignId('project_id')->nullable();
            $table->foreignId('expense_category_id')->nullable();
            $table->boolean('billable')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statement_entries', function (Blueprint $table) {
            $table->dropColumn('include');
            $table->dropColumn('employee_id');
            $table->dropColumn('project_id');
            $table->dropColumn('expense_category_id');
            $table->dropColumn('billable');
        });
    }
}
