<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutgoingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgoing_requests', function (Blueprint $table) {
            $table->id();
            $table->string('url',2000);
            $table->text('request');
            $table->string('method');
            $table->text('response');
            $table->string('status');
            $table->integer('code');
            $table->string('reason_phrase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgoing_requests');
    }
}
