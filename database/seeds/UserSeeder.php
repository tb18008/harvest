<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Matīss',
            'email' => 'matiss@iconcept.lv',
            'password' => Hash::make('e1OjkDdY9K3UwqrM'),
        ]);
    }
}
