<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BudgetTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('budget_types')->insert([
            'type' => 'project',
            'name' => 'Hours Per Project',
        ]);
        DB::table('budget_types')->insert([
            'type' => 'project_cost',
            'name' => 'Total Project Fees',
        ]);
        DB::table('budget_types')->insert([
            'type' => 'task',
            'name' => 'Hours Per Task',
        ]);
        DB::table('budget_types')->insert([
            'type' => 'task_fees',
            'name' => 'Fees Per Task',
        ]);
        DB::table('budget_types')->insert([
            'type' => 'person',
            'name' => 'Hours Per Person',
        ]);
        DB::table('budget_types')->insert([
            'type' => 'none',
            'name' => 'No Budget',
        ]);
    }
}
