<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HourlyRateTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hourly_rate_types')->insert([
            'type' => 'Project',
            'name' => 'Project Hourly Rate',
        ]);
        DB::table('hourly_rate_types')->insert([
            'type' => 'Tasks',
            'name' => 'Person Hourly Rate',
        ]);
        DB::table('hourly_rate_types')->insert([
            'type' => 'People',
            'name' => 'Task Hourly Rate',
        ]);
        DB::table('hourly_rate_types')->insert([
            'type' => 'none',
            'name' => 'No Hourly Rate',
        ]);
    }
}
