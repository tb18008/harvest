<?php

return [
    'employees_full' => 'App\Employee',
    'clients_full' => 'App\Client',
    'projects_full' => 'App\Project',
    'tasks_full' => 'App\Task',
    'time_entries_full' => 'App\TimeEntry',
    'expense_categories_full' => 'App\ExpenseCategory',
    'expenses_full' => 'App\Expense',
    'invoices_full' => 'App\Invoice',

    'employees_formatted' => 'employee',
    'clients_formatted' => 'client',
    'projects_formatted' => 'project',
    'tasks_formatted' => 'task',
    'time_entries_formatted' => 'time entry',
    'expense_categories_formatted' => 'expense category',
    'expenses_formatted' => 'expense',
    'invoices_formatted' => 'invoice',
    ];
