<?php

use Carbon\Carbon;

return [
    'week' => [
        'start_date' => Carbon::now()->startOfWeek(),
        'end_date' => Carbon::now()->endOfWeek()
    ],
    'semimonth' => [
        'start_date' => Carbon::now()->startOfMonth(),
        'end_date' => Carbon::now()->startOfMonth()->add('week',2)
    ],
    'month' => [
        'start_date' => Carbon::now()->startOfMonth(),
        'end_date' => Carbon::now()->endOfMonth()
    ],
    'quarter' => [
        'start_date' => Carbon::now()->startOfQuarter(),
        'end_date' => Carbon::now()->endOfQuarter()
    ],
    'year' => [
        'start_date' => Carbon::now()->startOfYear(),
        'end_date' => Carbon::now()->endOfYear()
    ],
];
