<?php

return [
    'request_head' => env('API_URL'),
    'request_tail' => env('API_TOKEN').env('API_ID'),

    'employees' => 'users',
    'employee' => 'user',
    'clients' => 'clients',
    'client' => 'client',
    'projects' => 'projects',
    'project' => 'project',
    'tasks' => 'tasks',
    'task' => 'task',
    'task_assignments' => 'task_assignments',
    'employee_assignments' => 'user_assignments',
    'time_entries' => 'time_entries',
    'time_entry' => 'time_entry',
    'expense_categories' => 'expense_categories',
    'expense_category' => 'expense_category',
    'expenses' => 'expenses',
    'expense' => 'expense',
    'invoices' => 'invoices',
    'invoice' => 'invoice',

    'response_user' => 'employee',
    'response_client' => 'client',
    'response_project' => 'project',
    'response_task' => 'task',
    'response_time_entry' => 'time_entry',
    'response_expense_category' => 'expense_category',
    'response_expense' => 'expense',
    'response_invoice' => 'invoice',
    'response_role' => 'role',
    'response_user_assignment' => 'user_assignment',

    'App\Project_sing' => 'project',
    'App\Project_plur' => 'projects',

    'table_task_assignments' => 'project_task',
    'table_employee_assignments' => 'employee_project',

];
