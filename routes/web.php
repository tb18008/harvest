<?php

use App\Events\EmployeeNotificationEvents\CheckUnfilledTimesheetsEvent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Disable registration
Auth::routes([
    'register' => false
]);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('home');
    });

    //Slack
    Route::get('slack/', function () {
        event(new CheckUnfilledTimesheetsEvent());
        return back();
    });

    //Employees
    Route::get('employees/', 'EmployeeController@index')->name('employees.index');
    Route::get('employees/create', 'EmployeeController@create')->name('employees.create');
    Route::post('employees/', 'EmployeeController@store')->name('employees.store');
    Route::get('employees/{employee}/edit', 'EmployeeController@edit')->name('employees.edit');
    Route::patch('employees/{employee}', 'EmployeeController@update')->name('employees.update');
    Route::delete('employees/{employee}', 'EmployeeController@destroy')->name('employees.destroy');

    //Clients
    Route::get('clients/', 'ClientController@index')->name('clients.index');
    Route::get('clients/create', 'ClientController@create')->name('clients.create');
    Route::post('clients/', 'ClientController@store')->name('clients.store');
    Route::get('clients/{client}/edit', 'ClientController@edit')->name('clients.edit');
    Route::patch('clients/{client}', 'ClientController@update')->name('clients.update');
    Route::delete('clients/{client}', 'ClientController@destroy')->name('clients.destroy');

    //Projects
    Route::get('projects/', 'ProjectController@index')->name('projects.index');
    Route::get('projects/create', 'ProjectController@create')->name('projects.create');
    Route::post('projects/', 'ProjectController@store')->name('projects.store');
    Route::get('projects/{project}/edit', 'ProjectController@edit')->name('projects.edit');
    Route::patch('projects/{project}', 'ProjectController@update')->name('projects.update');
    Route::delete('projects/{project}', 'ProjectController@destroy')->name('projects.destroy');
    Route::patch('projects/{project}/toggle_active', 'ProjectController@toggleActive')->name('projects.toggle_active');

    //Tasks
    Route::get('tasks/', 'TaskController@index')->name('tasks.index');
    Route::get('tasks/create', 'TaskController@create')->name('tasks.create');
    Route::post('tasks/', 'TaskController@store')->name('tasks.store');
    Route::get('tasks/{task}/edit', 'TaskController@edit')->name('tasks.edit');
    Route::patch('tasks/{task}', 'TaskController@update')->name('tasks.update');
    Route::delete('tasks/{task}', 'TaskController@destroy')->name('tasks.destroy');

    //TaskAssignments
    Route::get('projects/{project}/task_assignments', function($project){
        return \App\Project::find($project)->tasks;
    });
    //EmployeeAssignments
    Route::get('projects/{project}/employee_assignments', function($project){
        return \App\Project::find($project)->employees;
    });

    //TimeEntries
    Route::get('time_entries/', 'TimeEntryController@index')->name('time_entries.index');
    Route::get('time_entries/create', 'TimeEntryController@create')->name('time_entries.create');
    Route::post('time_entries/', 'TimeEntryController@store')->name('time_entries.store');
    Route::get('time_entries/{time_entry}/edit', 'TimeEntryController@edit')->name('time_entries.edit');
    Route::patch('time_entries/{time_entry}', 'TimeEntryController@update')->name('time_entries.update');
    Route::delete('time_entries/{time_entry}', 'TimeEntryController@destroy')->name('time_entries.destroy');

    //ExpenseCategories
    Route::get('expense_categories/', 'ExpenseCategoryController@index')->name('expense_categories.index');
    Route::get('expense_categories/create', 'ExpenseCategoryController@create')->name('expense_categories.create');
    Route::post('expense_categories/', 'ExpenseCategoryController@store')->name('expense_categories.store');
    Route::get('expense_categories/{expense_category}/edit', 'ExpenseCategoryController@edit')->name('expense_categories.edit');
    Route::patch('expense_categories/{expense_category}', 'ExpenseCategoryController@update')->name('expense_categories.update');
    Route::delete('expense_categories/{expense_category}', 'ExpenseCategoryController@destroy')->name('expense_categories.destroy');

    //Expenses
    Route::get('expenses/', 'ExpenseController@index')->name('expenses.index');
    Route::get('expenses/create', 'ExpenseController@create')->name('expenses.create');
    Route::post('expenses/', 'ExpenseController@store')->name('expenses.store');
    Route::get('expenses/{expense}/edit', 'ExpenseController@edit')->name('expenses.edit');
    Route::patch('expenses/{expense}', 'ExpenseController@update')->name('expenses.update');
    Route::delete('expenses/{expense}', 'ExpenseController@destroy')->name('expenses.destroy');

    Route::get('expenses_from_statement/{statement}/', 'ExpenseController@create_from_statement')->name('expenses.create_from_statement');
    Route::post('expenses_from_statement/', 'ExpenseController@store_from_statement')->name('expenses.store_from_statement');

    //Statements
    Route::post('statements/store', 'StatementController@store')->name('statements.store');

    //Invoices
    Route::get('invoices/', 'InvoiceController@index')->name('invoices.index');

    //Reports
    Route::get('reports/{report_type}/{start_date?}/{end_date?}', 'ReportController@show')->name('reports.show');

    //Harvest
    Route::get('harvest/store', 'HarvestController@store')->name('harvest.store');
    Route::get('harvest/update', 'HarvestController@update')->name('harvest.update');

});
