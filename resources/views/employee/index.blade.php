@extends('layouts.app')
@section('content')
<div class="row-12">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <b>
                    Employee index
                </b>
            </div>

            @include('messages')

            <div class="card-body">
                <table class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Employee ID</th>
                            <th scope="col">First name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Cost rate</th>
                            <th scope="col">Slack webhook</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{{$employee->id}}</td>
                                <td>{{$employee->first_name}}</td>
                                <td>{{$employee->last_name}}</td>
                                <td>{{$employee->email}}</td>
                                <td>{{$employee->cost_rate}}</td>
                                <td>{{$employee->slack_webhook}}</td>
                                <td>
                                    <div class="row pl-3 pr-3 justify-content-center">
                                        <a class="btn btn-secondary form-check-inline" href="{{url('/employees/'.$employee->id.'/edit')}}" role="button">Edit</a>
                                        <form method="POST" action="{{ url('employees/'.$employee->id) }}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-outline-danger">
                                                Delete
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td class="text-center" colspan="7">
                                <a href="{{url('/employees/create')}}" role="button" class="btn btn-outline-primary btn-lg btn-block">
                                    Add new employee ...
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="col-12 d-flex justify-content-center">
                    {{$employees->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
