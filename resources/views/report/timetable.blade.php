@inject('employee', 'App\Employee')
@inject('project', 'App\Project')

@if($report['items']->isNotEmpty())
    <div id="table-scroll" class="table-scroll">
        <table id="main-table" class="main-table table table-hover table-bordered">
            {{--COLUMN HEADERS--}}
            <thead>
                <tr class="text-white">
                    <th scope="col" class="bg-primary">Project</th>
                    @foreach($report['employees'] as $employee)
                        <th scope="col" class="bg-primary">
                            {{$employee['model']->first_name}} {{$employee['model']->last_name}}
                        </th>
                    @endforeach
                    <th scope="col" class="bg-primary">Total project hours</th>
                    {{--                    <th scope="col">Total project costs</th>--}}
                </tr>
            </thead>
            <tbody>
            {{--ROWS--}}
            @foreach($report['items'] as $item)
                <tr>
                    {{--PROJECT NAME AND PROGRESS--}}
                    <th scope="row" class="bg-light">
                        {{$item['project_name']}}<br>
                        @if(!$item['budget'])    {{--Without budget--}}
                        @php($no_budget = true)
                        <span>T&M (</span>{{number_format($item['spent'],2)}}<span>)</span>
                        @else    {{--With budget or with budget of 0 hours--}}
                        @php($no_budget = false)
                        {{number_format($item['spent'],2)}}/{{number_format($item['budget'],2)}}
                        @if($item['budget'] != 0)    {{--Percentage--}}
                        <span>(</span>
                        @if($item['spent']/$item['budget']>1) {{--Over budget--}}
                        <span class="text-danger">{{number_format($item['spent']/$item['budget']*100,2)}}%</span>
                        @else {{--Within budget--}}
                        <span>{{number_format($item['spent']/$item['budget']*100,2)}}%</span>
                        @endif
                        <span>)</span>
                        @endif
                        @endif
                    </th>
                    {{--EMPLOYEE HOURS IN PROJECT--}}

                    @foreach($report['employees'] as $employee)
                        @php($found = false)
                        @foreach($item['hours'] as $hour)
                            @if($employee['model']->id===$hour['employee_id'])
                                @if($no_budget==true)   {{--Project has no budget--}}
                                <td>
                                    {{number_format($hour['unpaid'],2)}}
                                </td>
                                @else   {{--Project has budget--}}
                                <td>{{number_format($hour['paid'],2)}} {{--paid--}}
                                    @if($hour['unpaid']!==0)
                                        <br>
                                        <span class="text-danger">-{{number_format($hour['unpaid'],2)}}</span> {{--unpaid--}}
                                    @endif
                                </td>
                                @endif
                                @php($found = true)
                                @break
                            @endif
                        @endforeach
                        @if(!$found) <td></td>
                        @endif
                    @endforeach

                    {{--PROJECT HOURS TOTAL--}}
                    @if($no_budget==true)   {{--Project has no budget--}}
                    <td>
                        {{number_format($item['unpaid'],2)}}
                    </td>
                    @else   {{--Project has budget--}}
                    <td class="bg-light">
                        <b>
                            @if($item['paid'] != 0)
                                {{number_format($item['paid'],2)}}
                            @endif
                        </b><br>
                        @if($item['unpaid'] != 0)
                            <span class="text-danger">
                            (-{{number_format($item['unpaid'],2)}})
                        </span>
                        @endif
                    </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            {{--EMPLOYEE HOURS TOTAL--}}
            <tr>
                <th scope="row" class="bg-light">Total employee hours</th>
                @foreach($report['employees'] as $employee)
                    <th class="bg-light">
                        <b>
                            @if($employee['paid']!=0)
                                {{number_format($employee['paid'],2)}}
                            @endif
                        </b><br>
                        @if($employee['unpaid'] != 0)
                            <span class="text-danger">
                                (-{{number_format($employee['unpaid'],2)}})
                            </span>
                        @endif
                    </th>
                @endforeach

                {{--HOURS TOTAL--}}
                <th class="bg-light">
                    <b>
                        @if($report['total']['paid'] != 0)
                            {{number_format($report['total']['paid'],2)}}
                        @endif
                    </b><br>
                    @if($report['total']['unpaid'] != 0)
                        <span class="text-danger">
                            (-{{number_format($report['total']['unpaid'],2)}})
                        </span>
                    @endif
                </th>
            </tr>
            </tfoot>
        </table>
    </div>
@else
    <p class="card-text">No entries during this time period</p>
@endif
