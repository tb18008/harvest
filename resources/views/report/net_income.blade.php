@inject('employee', 'App\Employee')
@inject('project', 'App\Project')

@if($report['items']->isNotEmpty())
    <div class="col-3">
        <div class="card mb-3">
            <h5 class="card-header">Summary</h5>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">
                            Total invoiced
                        </div>
                        <div class="col text-right">
                        <span class="text-success">
                            @if($report['totals']['invoiced'] !== 0)
                                {{number_format($report['totals']['invoiced'],2)}}
                                @lang('units.currency')
                            @endif
                        </span>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">
                            Total labor costs
                        </div>
                        <div class="col text-right">
                            <span class="text-danger">
                                @if($report['totals']['time_entries'] !== 0)
                                    -{{number_format($report['totals']['time_entries'],2)}}
                                    @lang('units.currency')
                                @endif
                            </span>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col">
                            Total expenses
                        </div>
                        <div class="col text-right">
                            <span class="text-danger">
                                @if($report['totals']['expenses'] !== 0)
                                    -{{number_format($report['totals']['expenses'],2)}}
                                    @lang('units.currency')
                                @endif
                            </span>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        Net income
                    </div>
                    <div class="col text-right">
                        @if($report['totals']['invoiced'] - $report['totals']['time_entries'] - $report['totals']['expenses'] < 0)
                            <span class="text-danger font-weight-bold">
                                {{number_format($report['totals']['invoiced'] - $report['totals']['time_entries'] - $report['totals']['expenses'],2)}} @lang('units.currency')
                            </span>
                        @endif
                        @if($report['totals']['invoiced'] - $report['totals']['time_entries'] - $report['totals']['expenses'] > 0)
                            <span class="text-success font-weight-bold">
                                {{number_format($report['totals']['invoiced'] - $report['totals']['time_entries'] - $report['totals']['expenses'],2)}} @lang('units.currency')
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="accordion" id="net_income_accordion">
        @foreach($report['items'] as $client)
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse_{{$client['client']->id}}" aria-expanded="true" aria-controls="collapse_{{$client['client']->id}}">
                            {{$client['client']->name}}
                        </button>
                    </h2>
                </div>

                <div id="collapse_{{$client['client']->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#net_income_accordion">
                    <div class="card-body">
                        <div>
                            <table id="main-table" class="main-table table table-hover table-bordered">
                                {{--COLUMN HEADERS--}}
                                <thead>
                                <tr class="text-white">
                                    <th scope="col" class="bg-primary">Project</th>
                                    <th scope="col" class="bg-primary" title="Project's invoiced amount during selected period">Income</th>
                                    <th scope="col" class="bg-primary" title="All of project's time entries × respective employee's cost rate">Labor costs</th>
                                    <th scope="col" class="bg-primary" title="All of project's billable expenses">Expenses</th>
                                    <th scope="col" class="bg-primary">Net income</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--ROWS--}}
                                @foreach($client['projects'] as $project)
                                    <tr>
                                        <th scope="row" class="bg-light">
                                            {{$project['project']->name}}<br>
                                        </th>
                                        <td>
                                            <span class="text-success">
                                                @if($project['invoiced'] !== 0)
                                                    {{number_format($project['invoiced'],2)}}
                                                    @lang('units.currency')
                                                @endif
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text-danger">
                                                @if($project['time_entries'] !== 0)
                                                    -{{number_format($project['time_entries'],2)}}
                                                    @lang('units.currency')
                                                @endif
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text-danger">
                                                @if($project['expenses'] !== 0)
                                                    -{{number_format($project['expenses'],2)}}
                                                    @lang('units.currency')
                                                @endif
                                            </span>
                                        </td>
                                        <th class="bg-light">
                                            @if($project['invoiced'] - $project['time_entries'] - $project['expenses'] < 0)
                                                <span class="text-danger font-weight-bold">
                                                    {{number_format($project['invoiced'] - $project['time_entries'] - $project['expenses'],2)}} @lang('units.currency')
                                                </span>
                                            @endif
                                            @if($project['invoiced'] - $project['time_entries'] - $project['expenses'] > 0)
                                                <span class="text-success font-weight-bold">
                                                    {{number_format($project['invoiced'] - $project['time_entries'] - $project['expenses'],2)}} @lang('units.currency')
                                                </span>
                                            @endif
                                        </th>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th scope="row" class="bg-light">
                                        {{$client['client']->name}} total
                                    </th>
                                    <th class="bg-light">
                                        <span class="text-success">
                                            @if($client['totals']['invoiced'] !== 0)
                                                {{number_format($client['totals']['invoiced'],2)}}
                                                @lang('units.currency')
                                            @endif
                                        </span>
                                    </th>
                                    <th class="bg-light">
                                        <span class="text-danger">
                                            @if($client['totals']['time_entries'] !== 0)
                                                -{{number_format($client['totals']['time_entries'],2)}}
                                                @lang('units.currency')
                                            @endif
                                        </span>
                                    </th>
                                    <th class="bg-light">
                                        <span class="text-danger">
                                            @if($client['totals']['expenses'] !== 0)
                                                -{{number_format($client['totals']['expenses'],2)}}
                                                @lang('units.currency')
                                            @endif
                                        </span>
                                    </th>
                                    <th class="bg-light">
                                        @if($client['totals']['invoiced'] - $client['totals']['time_entries'] - $client['totals']['expenses'] < 0)
                                            <span class="text-danger font-weight-bold">
                                                {{number_format($client['totals']['invoiced'] - $client['totals']['time_entries'] - $client['totals']['expenses'],2)}} @lang('units.currency')
                                            </span>
                                        @endif
                                        @if($client['totals']['invoiced'] - $client['totals']['time_entries'] - $client['totals']['expenses'] > 0)
                                            <span class="text-success font-weight-bold">
                                                {{number_format($client['totals']['invoiced'] - $client['totals']['time_entries'] - $client['totals']['expenses'],2)}} @lang('units.currency')
                                            </span>
                                        @endif
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>


@else
    <p class="card-text">No entries during this time period</p>
@endif
