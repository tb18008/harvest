<div class="col">
    <div class="row">

        {{--    PREV    --}}
        <div id="prev_form">
            <form method="GET" action="{{ url('reports/'.$report['report_type']) }}">
                <input type="hidden" name="period_select" value="{{$report['time_type']}}">
                <input type="hidden" name="prev_next" value="prev">
                <input type="hidden" name="start_date" value="{{$report['start_date']}}">
                <input type="hidden" name="end_date" value="{{$report['end_date']}}">
                <button type="submit" class="btn btn-primary">Previous</button>
            </form>
        </div>
        {{--    DROPDOWN    --}}
        <div class="dropdown pl-2">
            <form method="GET" action="{{ url('reports/'.$report['report_type']) }}" id="periodForm">
                <select id="period_select" name="period_select" onchange="submitPeriod()">
                    <option value="week">Week</option>
                    <option value="semimonth">Semimonth</option>
                    <option value="month">Month</option>
                    <option value="quarter">Quarter</option>
                    <option value="year">Year</option>
                    <option value="all">All time</option>
                    <option value="custom">Custom</option>
                </select>
            </form>
        </div>
        {{--    NEXT    --}}
        <div id="next_form" class="pl-2">
            <form method="GET" action="{{ url('reports/'.$report['report_type']) }}">
                <input type="hidden" name="period_select" value="{{$report['time_type']}}">
                <input type="hidden" name="prev_next" value="next">
                <input type="hidden" name="start_date" value="{{$report['start_date']}}">
                <input type="hidden" name="end_date" value="{{$report['end_date']}}">
                <button type="submit" class="btn btn-primary">Next</button>
            </form>
        </div>
        <div class="col ml-2 mr-2" id="custom_picker" style="display:none">
            <form method="GET" action="{{ url('reports/'.$report['report_type']) }}">
                <div class="row">
                    <p>From: <input type="text" id="start_date" name="start_date" value="{{Carbon\Carbon::now()->startOfMonth()->format('Y-m-d')}}"></p>
                    <p class="ml-2">To: <input type="text" id="end_date" name="end_date" value="{{ Carbon\Carbon::now()->endOfMonth()->format('Y-m-d')}}"></p>
                    <input type="hidden" name="period_select" value = "custom">
                    <button type="submit" class="btn btn-primary pr-3 pl-3 pt-0 pb-0 mr-1 ml-2">Show</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $( "#start_date" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $( "#end_date" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $( "#period_select" ).val("{{$report['time_type']}}");

        if ("{{$report['time_type']}}"==='custom'){
            @isset($report['start_date'])
            $( "#start_date" ).val("{{Carbon\Carbon::parse($report['start_date'])->format('Y-m-d')}}");
            $( "#end_date" ).val("{{Carbon\Carbon::parse($report['end_date'])->format('Y-m-d')}}");
            @endisset
            document.getElementById("custom_picker").style.display = "block";
            document.getElementById("prev_form").style.display = "none";
            document.getElementById("next_form").style.display = "none";
        }
        if ("{{$report['time_type']}}"==='all'){
            document.getElementById("prev_form").style.display = "none";
            document.getElementById("next_form").style.display = "none";
        }
    });
    function submitPeriod() {
        custom_picker = document.getElementById("custom_picker");
        if(document.getElementById("period_select").value !== 'custom'){
            document.getElementById("periodForm").submit();
            custom_picker.style.display = "none";
        }
        else {
            custom_picker.style.display = "block";
        }
        document.getElementById("prev_form").style.display = "none";
        document.getElementById("next_form").style.display = "none";
    }
</script>

