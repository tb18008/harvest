@extends('layouts.app')

@section('content')
    <div class="row-12">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <b>
                                @lang('messages.'.$report['report_type'])
                                @if(array_key_exists ('start_date',$report))
                                    @if($report['start_date'])
                                        ({{$report['start_date']->toDateString()}} - {{$report['end_date']->toDateString()}})
                                    @endif
                                @endif
                            </b>
                        </div>
                        <div class="float-right">
                            @include('report.date_picker')
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('report.'.$report['report_type'])
                </div>
            </div>
        </div>
    </div>
@endsection
