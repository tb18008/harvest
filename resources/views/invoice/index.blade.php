@extends('layouts.app')
@section('content')
<div class="row-12">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <b>
                    Invoice index
                </b>
            </div>

            @include('messages')

            <div class="card-body">
                <table class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Invoice ID</th>
                            <th scope="col">Client</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Issue date</th>
                            <th scope="col">Period</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>{{$invoice->id}}</td>
                                <td>{{$invoice->client->name}}</td>
                                <td>{{$invoice->amount}} @lang('units.currency')</td>
                                <td>{{Carbon\Carbon::parse($invoice->issue_date)->toDateString()}}</td>
                                @if($invoice->period_start)
                                    <td>{{Carbon\Carbon::parse($invoice->period_start)->toDateString()}} - {{Carbon\Carbon::parse($invoice->period_end)->toDateString()}}</td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="col-12 d-flex justify-content-center">
                    {{$invoices->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
