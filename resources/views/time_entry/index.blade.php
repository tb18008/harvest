@extends('layouts.app')
@section('content')
    <div class="row-12">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <b>
                        Time entry index
                    </b>
                </div>

                @include('messages')

                <div class="card-body">
                    <table class="table table-hover table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Time Entry ID</th>
                    <th scope="col">Employee</th>
                    <th scope="col">Project</th>
                    <th scope="col">Task</th>
                    <th scope="col">Hours</th>
                    <th scope="col">Spent date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($time_entries as $time_entry)
                    <tr>
                        <td>{{$time_entry->id}}</td>
                        <td>{{$time_entry->employee->first_name.' '.$time_entry->employee->last_name}}</td>
                        <td>{{$time_entry->project->name}}</td>
                        <td>{{$time_entry->task->name}}</td>
                        <td>{{$time_entry->hours}}</td>
                        <td>{{Carbon\Carbon::parse($time_entry->spent_date)->toDateString()}}</td>
                        <td>
                            <div class="row justify-content-center">
                                @if($time_entry->project->is_active)
                                    <a class="btn btn-secondary form-check-inline" role="button"  href="{{url('/time_entries/'.$time_entry->id.'/edit')}}">Edit</a>
                                @else
                                    <form method="GET" action="{{ url('time_entries/'.$time_entry->id).'/edit' }}">
                                        @method('GET')
                                        @csrf
                                        <button type="submit" disabled title="{{config('harvest.edit_time_entry_archived')}}" class="btn btn-outline-secondary form-check-inline">
                                            Edit
                                        </button>
                                    </form>
                                @endif
                                <form method="POST" action="{{ url('time_entries/'.$time_entry->id) }}">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" @if(!$time_entry->project->is_active) disabled title="{{config('harvest.delete_time_entry_archived')}}" @endif class="btn btn-outline-danger form-check-inline">
                                        Delete
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td class="text-center" colspan="7">
                        <a href="{{url('/time_entries/create')}}" role="button" class="btn btn-outline-primary btn-lg btn-block">
                            Add new time entry ...
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    {{$time_entries->links()}}
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
