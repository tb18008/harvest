@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><b>Add new time entry</b></div>

                    @include('messages')

                    <form class="m-2" method="POST" action="{{ url('time_entries/') }}">

                        @csrf
                        @method('post')

                        <div class="form-group row">
                            <label for="employee_id" class="col-md-4 col-form-label text-md-right">Employee</label>

                            <div class="col-md-6">
                                <select id="user_id" name="user_id" disabled class="form-control @error('user_id') is-invalid @enderror" autocomplete="user_id" autofocus>
                                    <option value="" selected disabled></option>
                                </select>
                                @error('user_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="project_id" class="col-md-4 col-form-label text-md-right">Project</label>

                            <div class="col-md-6">
                                <select id="project_id" name="project_id"  class="form-control @error('project_id') is-invalid @enderror" required autocomplete="project_id" autofocus>
                                    <option value="" selected disabled>Select project</option>
                                    @foreach($projects as $project)
                                        <option value="{{$project->id}}" @if(!$project->is_active) disabled @endif>{{$project->name}}</option>
                                    @endforeach
                                </select>
                                @error('project_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="task_id" class="col-md-4 col-form-label text-md-right">Task</label>

                            <div class="col-md-6">
                                <select id="task_id" name="task_id" disabled class="form-control @error('task_id') is-invalid @enderror" required autocomplete="task_id" autofocus>
                                    <option value="" selected disabled></option>
                                </select>
                                @error('task_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hours" class="col-md-4 col-form-label text-md-right">Hours</label>

                            <div class="col-md-6">
                                <input id="hours" type="number" min="0" class="form-control @error('hours') is-invalid @enderror" name="hours" value="" autocomplete="hours" autofocus>
                                @error('hours')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="spent_date" class="col-md-4 col-form-label text-md-right">Spent date</label>

                            <div class="col-md-6">
                                <input id="date_input" type="text" class="form-control @error('spent_date') is-invalid @enderror" name="date_input" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" required autocomplete="date_input" autofocus>
                                <input id="spent_date" type="hidden" class="form-control" name="spent_date" value="{{Carbon\Carbon::now()->format('Y-m-d\TH:i:sO')}}">
                                @error('spent_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group d-flex flex-row-reverse">
                            <div class="col-md-3 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Add time entry
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/date_input_formatting.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/project_employee_assignments.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/project_task_assignments.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/array_pluck.js') }}"></script>
@stop
