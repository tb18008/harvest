@extends('layouts.app')
@section('content')
<div class="row-12">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <b>
                    Expense index
                </b>
            </div>

            @include('messages')

            <div class="card-body">
                <table class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Expense ID</th>
                            <th scope="col">Employee</th>
                            <th scope="col">Project</th>
                            <th scope="col">Expense category</th>
                            <th scope="col">Spent date</th>
                            <th scope="col">Billable</th>
                            <th scope="col">Total cost</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($expenses as $expense)
                            <tr>
                                <td>{{$expense->id}}</td>
                                <td>{{$expense->employee->first_name.' '.$expense->employee->last_name}}</td>
                                <td>{{$expense->project->name}}</td>
                                <td>{{$expense->expense_category->name}}</td>
                                <td>{{Carbon\Carbon::parse($expense->spent_date)->toDateString()}}</td>
                                <td class="text-center">
                                    @if($expense->billable)
                                        <input type="checkbox" checked="checked" onclick="return false;">
                                    @else
                                        <input type="checkbox" onclick="return false;">
                                    @endif
                                </td>
                                <td>{{$expense->total_cost}} @lang('units.currency')</td>
                                <td>
                                    <div class="row pl-3 pr-3 justify-content-center">
                                        <a class="btn btn-secondary form-check-inline" href="{{url('/expenses/'.$expense->id.'/edit')}}" role="button">Edit</a>
                                        <form method="POST" action="{{ url('expenses/'.$expense->id) }}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-outline-danger">
                                                Delete
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td class="text-center" colspan="8">
                                <a href="{{url('/expenses/create')}}" role="button" class="btn btn-outline-primary btn-lg btn-block">
                                    Add new expense ...
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="8">
                                <a href="#" data-toggle="modal" data-target="#file_input" role="button" class="btn btn-outline-primary btn-lg btn-block">
                                    Import expenses ...
                                </a>

                                <!-- Modal -->
                                <div class="modal fade" id="file_input" tabindex="-1" role="dialog" aria-labelledby="file_input_label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="file_input_label">Select statement file</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form class="m-2" method="POST" action="{{ route('statements.store') }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="input-group">
                                                        <div class="custom-file">
                                                            @if($errors->has('csv_file'))
                                                                <span class="invalid-feedback mr-2" role="alert">
                                                                    <strong>{{ $errors->first('csv_file') }}</strong>
                                                                </span>
                                                            @endif
                                                            <input type="file" class="custom-file-input @error('csv_file') is-invalid @enderror" id="csv_file" name="csv_file" autocomplete="csv_file" value="" required autofocus>
                                                            <label class="custom-file-label" id="csv_file_label" for="csv_file">Choose file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}">
                                                <div class="modal-footer">
                                                    <button class="btn btn-outline-primary" type="submit">Upload</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="col-12 d-flex justify-content-center">
                    {{$expenses->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#csv_file').change(function(e){
            $('#csv_file_label').html(e.target.files[0].name);
        });
    </script>
@stop
