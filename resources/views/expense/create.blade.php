@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"><b>Add new expense</b></div>

                    @include('messages')

                    <form class="m-2" method="POST" action="{{ url('expenses/') }}">

                        @csrf

                        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">Employee</label>

                            <div class="col-md-6">
                                <select id="user_id" name="user_id" disabled class="form-control @error('user_id') is-invalid @enderror" autocomplete="user_id" autofocus required>
                                    <option value="" selected disabled></option>
                                </select>
                                @error('user_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="project_id" class="col-md-4 col-form-label text-md-right">Project</label>

                            <div class="col-md-6">
                                <select id="project_id" name="project_id"  class="form-control @error('project_id') is-invalid @enderror" name="project_id" required autocomplete="project_id" autofocus>
                                    <option value="" selected disabled>Select project</option>
                                    @foreach($projects as $project)
                                        <option value="{{$project->id}}" @if(!$project->is_active) disabled @endif>{{$project->name}}</option>
                                    @endforeach
                                </select>
                                @error('project_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="expense_category_id" class="col-md-4 col-form-label text-md-right">Expense category</label>

                            <div class="col-md-6">
                                <select id="expense_category_id" name="expense_category_id" class="form-control @error('expense_category_id') is-invalid @enderror" required autocomplete="expense_category_id" autofocus>
                                    <option value="" selected disabled>Select expense category</option>
                                    @foreach($expense_categories as $expense_category)
                                        <option value="{{$expense_category->id}}">{{$expense_category->name}}</option>
                                    @endforeach
                                </select>
                                @error('expense_category_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="spent_date" class="col-md-4 col-form-label text-md-right">Spent date</label>

                            <div class="col-md-6">
                                <input id="date_input" type="text" class="form-control @error('spent_date') is-invalid @enderror" name="date_input" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" required autocomplete="spent_date" autofocus>
                                <input id="spent_date" type="hidden" class="form-control" name="spent_date" value="{{Carbon\Carbon::now()->format('Y-m-d\TH:i:sO')}}">
                                @error('spent_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        {{--BILLABLE CHECKBOX--}}
                        <div class="form-group row d-flex align-items-center">
                            <label for="billable_checkbox" class="col-md-4 col-form-label text-md-right">Billable</label>

                            <div class="col ml-2">
                                <input type="checkbox" class="checkbox-inline" id="billable_checkbox" checked>
                                <input type="hidden" name="billable" value="1" id="billable">
                            </div>
                        </div>

                        {{--TOTAL COST INPUT--}}
                        <div class="form-group row">
                            <label for="total_cost" class="col-md-4 col-form-label text-md-right">Total cost</label>

                            <div class="col-md-5">
                                <input id="total_cost" type="number" min="0" step="0.01" class="form-control mr-2 @error('total_cost') is-invalid @enderror" name="total_cost" value="" autocomplete="total_cost" autofocus>
                                @error('total_cost')
                                <span class="invalid-feedback mr-2" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col d-flex align-items-center">
                                <label class="form-check-label" for="total_cost">@lang('units.currency')</label>
                            </div>
                        </div>

                        <div class="form-group d-flex flex-row-reverse">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    Add expense
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/billable_checkbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/date_input_formatting.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/project_employee_assignments.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/array_pluck.js') }}"></script>
@stop
