@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col">
                            <div class="card mb-3">
                                {{--FIRST COLUMN--}}
                                <div class="card-header">
                                    Reports
                                </div>
                                <ul class="list-group list-group-flush">
                                    {{--Last week's timetable--}}
                                    <li class="list-group-item">
                                        <a href="{{url('reports/timetable?period_select=week&prev_next=prev&start_date='.Carbon\Carbon::now()->startOfWeek().'&end_date='.Carbon\Carbon::now()->endOfWeek())}}">
                                            Timetable
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="{{url('reports/net_income?period_select=month&prev_next=prev')}}">
                                            Net income
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    Notifications
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href="{{url('slack/')}}">Send Slack notifications</a></li>
                                </ul>
                            </div>
                        </div>

                        {{--SECOND COLUMN--}}
                        <div class="col">
                            <div class="card mb-3">
                                <div class="card-header">
                                    Indexes
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href="{{url('employees')}}">Employees</a></li>
                                    <li class="list-group-item"><a href="{{url('projects')}}">Projects</a></li>
                                    <li class="list-group-item"><a href="{{url('time_entries')}}">Time entries</a></li>
                                    <li class="list-group-item"><a href="{{url('expenses')}}">Expenses</a></li>
                                    <li class="list-group-item"><a href="{{url('invoices')}}">Invoices</a></li>
                                </ul>
                            </div>

                            <div class="card mb-3">
                                <div class="card-header">
                                    Match with harvest
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href="{{url('harvest/update')}}">Routine update</a></li>
                                    @if(DB::table('jobs')->count())
                                        <li class="list-group-item">{{'Database will be matched with Harvest in '.Carbon\Carbon::now()->setTimezone('Europe/Riga')->add(1,'days')->startOfDay()->diffForHumans()}}</li>
                                    @else
                                        <li class="list-group-item"><a href="{{url('harvest/store')}}">Refresh all</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>

                        {{--THIRD COLUMN--}}
                        <div class="col">
                            <div class="card mb-3">
                                <div class="card-header">
                                    Management
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href="{{url('clients')}}">Clients</a></li>
                                    <li class="list-group-item"><a href="{{url('tasks')}}">Tasks</a></li>
                                    <li class="list-group-item"><a href="{{url('expense_categories')}}">Expense categories</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
