@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Edit project '{{$project->name}}'</b></div>

                    @include('messages')

                    <form class="m-2" method="POST" action="{{ route('projects.update',['project'=>$project])}}">

                        @csrf
                        @method('patch')

                        {{--CLIENT ROW--}}
                        <div class="form-group row">
                            <label for="client_id" class="col-md-2 col-form-label text-md-right">Client</label>

                            <div class="col-md-10">
                                <select id="client_id" name="client_id" class="form-control @error('client_id') is-invalid @enderror" name="client_id" required autocomplete="client_id" autofocus>
                                    <option value=""></option>
                                    @foreach($clients as $client)
                                        <option value="{{$client->id}}">{{$client->name}}</option>
                                    @endforeach
                                </select>
                                @error('client_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        {{--NAME ROW--}}
                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Project name</label>

                            <div class="col-md-10">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$project->name}}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        {{--BILLING ROW--}}
                        <div class="form-group row">
                            <label for="hourly_rate" class="col-md-2 col-form-label text-md-right">Billing type</label>

                            <div class="col-md-10 d-flex align-items-center">
                                <div class="container">
                                    <div class="row">
                                        {{--BILLABLE CHECKBOX--}}
                                        <div class="col-md-1 form-check-inline">
                                            <input type="checkbox" class="form-check-input" id="billable_checkbox" @if($project->is_billable) checked @endif>
                                            <input type="hidden" name="is_billable" @if($project->is_billable) value="1" @else value="0" @endif id="is_billable">
                                            <label class="form-check-label" for="billable_checkbox">Billable</label>
                                        </div>

                                        <div class="col-md-10" style="display:@if($project->is_billable) block @else none @endif" id="form_billable">
                                            <div class="row">
                                                {{--FIXED FEE / HOURLY RATE SELECT--}}
                                                <div class="col-md-3" id="is_fixed_fee_select">
                                                    <select class="form-control" name="is_fixed_fee" id="is_fixed_fee">
                                                        <option value="0" >Hourly Rate</option>
                                                        <option value="1" @if($project->is_fixed_fee) selected @endif>Fixed Fee</option>
                                                    </select>
                                                </div>

                                                {{--FIXED FEE INPUT--}}
                                                <div class="col-md-8" style="display:@if($project->is_billable && $project->is_fixed_fee) block @else none @endif" id="fixed_fee_input">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <input id="fee" type="number" min="0" class="form-control mr-2 @error('fee') is-invalid @enderror" name="fee" value="{{$project->fee}}" autocomplete="fee" autofocus>
                                                            @error('fee')
                                                            <span class="invalid-feedback mr-2" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                        <div class="col d-flex align-items-center">
                                                            <label class="form-check-label" for="fee">@lang('units.currency')</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-8" style="display:@if(!$project->is_fixed_fee) block @else none @endif" id="hourly_rate_input">
                                                    <div class="row">

                                                        {{--HOURLY RATE SELECT--}}
                                                        <div class="col-5">
                                                            <select class="form-control" id="bill_by" name="bill_by">
                                                                <option value="none" @if($project->hourly_rate_type->type == 'none') selected @endif>No Hourly Rate</option>
                                                                <option disabled>──────────</option>
                                                                <option value="Project" @if($project->hourly_rate_type->type == 'Project') selected @endif>Project Hourly Rate</option>
                                                                {{--<option value="Tasks">Person Hourly Rate</option>--}}
                                                                {{--<option value="People">Task Hourly Rate</option>--}}
                                                            </select>
                                                        </div>

                                                        {{--HOURLY RATE INPUT--}}
                                                        <div class="col-7" style="display:@if($project->hourly_rate_type->type == 'Project') block @else none @endif" id="rate_input">
                                                            <div class="row">
                                                                <input id="hourly_rate" type="number" min="0" class="col-md-6 form-control @error('hourly_rate') is-invalid @enderror" name="hourly_rate" value="{{$project->hourly_rate}}" autocomplete="hourly_rate" autofocus>
                                                                @error('hourly_rate')
                                                                <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                                <div class="col d-flex align-items-center">
                                                                    <label class="form-check-label" for="hourly_rate">@lang('units.currency')@lang('units.per')@lang('units.hour')</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--BUDGET ROW--}}
                        <div class="form-group row">
                            <label for="budget_by" class="col-md-2 col-form-label text-md-right">Budget type</label>
                            <div class="col-md-3">
                                <select class="form-control collapse @if($project->is_billable) show @endif" id="billable_budget_by">
                                    <option value="none" @if($project->budget_type->type == 'none') selected @endif>No Budget</option>
                                    <option disabled>──────────</option>
                                    <option value="project_cost" @if($project->budget_type->type == 'project_cost') selected @endif>Total Project Fees</option>
                                    <option value="task_fees" @if($project->budget_type->type == 'task_fees') selected @endif>Fees Per Task</option>
                                    <option disabled>──────────</option>
                                    <option value="project" @if($project->budget_type->type == 'project') selected @endif>Total Project Hours</option>
{{--                                    <option value="task">Hours Per Task</option>--}}
                                    <option value="person" @if($project->budget_type->type == 'person') selected @endif>Hours Per Person</option>
                                </select>
                                <select class="form-control collapse @if(!$project->is_billable) show @endif" id="non_billable_budget_by">
                                    <option value="none" @if($project->budget_type->type == 'none') selected @endif>No Budget</option>
                                    <option disabled>──────────</option>
                                    <option value="project" @if($project->budget_type->type == 'project') selected @endif>Total Project Hours</option>
{{--                                    <option value="task">Hours Per Task</option>--}}
                                    <option value="person" @if($project->budget_type->type == 'person') selected @endif>Hours Per Person</option>
                                </select>
                                <input type="hidden" id="budget_by" name="budget_by" value="{{$project->budget_type->type}}">
                            </div>
                            <div class="form-check-inline col-md-2" style="display:@if($project->budget_type->type != 'none') block @else none @endif" id="budget_input">
                                <div class="row">
                                    <div class="col-8">
                                        <input id="budget" type="number" min="0" class="form-control @error('budget') is-invalid @enderror" name="budget" value="{{$project->budget}}" autocomplete="budget" autofocus>
                                        @error('budget')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-4 d-flex align-items-center">
                                        <span class="form-check-label">@lang('units.hours')</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            {{--ASIGNED TASKS--}}
                            <label for="task_assignments" class="col-md-2 col-form-label text-md-right">Assigned tasks</label>
                            <div class="col-md-3">
                                <select id="task_assignments" class="form-control" name="task_assignments[]" multiple>
                                    @foreach($tasks as $task)
                                        <option value="{{$task->id}}" @if($project->tasks->find($task->id)) selected @endif>{{$task->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--ASIGNED EMPLOYEES--}}
                            <label for="user_assignments" class="col-md-2 col-form-label text-md-right">Assigned employees</label>
                            <div class="col-md-3">
                                <select id="user_assignments" class="form-control" name="user_assignments[]" multiple>
                                    @foreach($employees as $employee)
                                        <option value="{{$employee->id}}" @if($project->employees->find($employee->id)) selected @endif>{{$employee->first_name.' '.$employee->last_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group d-flex flex-row-reverse">
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary">
                                    Update project
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#client_id").val({{$project->client_id}});   //Set client
        });
    </script>
    <script type="text/javascript" src="{{ asset('js/project_type_form.js') }}"></script>
@stop
