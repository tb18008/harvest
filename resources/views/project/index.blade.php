@extends('layouts.app')
@section('content')

<div class="row-12">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <b>
                    Project index
                </b>
            </div>

            @include('messages')

            <div class="card-body">
                <table class="table table-hover table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Project ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Client</th>
                        <th scope="col">Billable</th>
                        <th scope="col">Fixed fee</th>
                        <th scope="col">Fee</th>
                        <th scope="col">Hourly rate type</th>
                        <th scope="col">Hourly rate</th>
                        <th scope="col">Budget type</th>
                        <th scope="col">Budget</th>
                        <th scope="col">Archived</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td>{{$project->id}}</td>
                            <td>{{$project->name}}</td>
                            <td>{{$project->client->name}}</td>
                            <td class="text-center">
                                @if($project->is_billable)
                                    <input type="checkbox" checked="checked" onclick="return false;">
                                @else
                                    <input type="checkbox" onclick="return false;">
                                @endif
                            </td>
                            <td class="text-center">
                                @if($project->is_fixed_fee)
                                    <input type="checkbox" checked="checked" onclick="return false;">
                                @else
                                    <input type="checkbox" onclick="return false;">
                                @endif
                            </td>
                            <td>{{$project->fee}}</td>
                            <td>{{$project->hourly_rate_type->name}}</td>
                            <td>{{$project->hourly_rate}}</td>
                            <td>{{$project->budget_type->name}}</td>
                            <td>{{$project->budget}}</td>
                            <td class="text-center">
                                @if($project->is_active)
                                    <input type="checkbox" onclick="return false;">
                                @else
                                    <input type="checkbox" checked="checked" onclick="return false;">
                                @endif
                            </td>
                            <td class="center-block text-center">
                                <a class="btn btn-secondary mb-2" href="{{url('/projects/'.$project->id.'/edit')}}" role="button">Edit</a>
                                <form class="mb-2" method="POST" action="{{ route('projects.toggle_active',['project'=>$project])}}">
                                    @method('PATCH')
                                    @csrf
                                    <input type="hidden" name="is_active" @if($project->is_active) value="0" @else value="1" @endif id="is_active">
                                    <button type="submit" @if($project->is_active) class="btn btn-warning" @else class="btn btn-info" @endif title="Archiving a project will make it inactive until restoration">
                                        @if($project->is_active)
                                            Archive
                                        @else
                                            Restore
                                        @endif
                                    </button>
                                </form>
                                <form method="POST" action="{{ url('projects/'.$project->id) }}">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger" title="Deleting a project will delete any time entries or expenses tracked to it (but not invoices)">
                                        Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td class="text-center" colspan="12">
                            <a href="{{url('/projects/create')}}" role="button" class="btn btn-outline-primary btn-lg btn-block">
                                Add new project ...
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="col-12 d-flex justify-content-center">
                    {{$projects->links()}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
