@extends('layouts.app')
@section('content')
<div class="row-12">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <b>
                    Task index
                </b>
            </div>

            @include('messages')

            <div class="card-body">
                <table class="table table-hover table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Task ID</th>
                            <th scope="col">Task name</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{$task->id}}</td>
                                <td>{{$task->name}}</td>
                                <td>
                                    <div class="row pl-3 pr-3 justify-content-center">
                                        <a class="btn btn-secondary form-check-inline" href="{{url('/tasks/'.$task->id.'/edit')}}" role="button">Edit</a>
                                        <form method="POST" action="{{ url('tasks/'.$task->id) }}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-outline-danger">
                                                Delete
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td class="text-center" colspan="3">
                                <a href="{{url('/tasks/create')}}" role="button" class="btn btn-outline-primary btn-lg btn-block">
                                    Add new task ...
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="col-12 d-flex justify-content-center">
                    {{$tasks->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
