@extends('layouts.app')
@section('content')
<div class="row-12">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <b>
                  Import expenses
                </b>
            </div>

            <div class="card-body">

                @include('messages')

                <form class="m-2" method="POST" action="{{ url('expenses_from_statement/') }}">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="col-auto">
                            <table class="table table-hover table-active table-bordered table-responsive">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Include</th>
                                    <th scope="col">Spent date</th>
                                    <th scope="col">Client</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Employee</th>
                                    <th scope="col">Project</th>
                                    <th scope="col">Expense category</th>
                                    <th scope="col">Billable</th>
                                    <th scope="col">Save as default</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($statement_entries as $statement_entry)
                                    <tr>
                                        <td>
                                            <div class="col ml-2">
                                                <input type="checkbox" class="include_checkbox checkbox-inline @error($statement_entry->id.'[include]') is-invalid @enderror" id="expenses[{{$statement_entry->id}}][include_checkbox]" @if($statement_entry->include) checked @endif>
                                                @error($statement_entry->id.'[include]')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <input type="hidden" name="expenses[{{$statement_entry->id}}][include]" value="{{$statement_entry->include? 1 : 0}}" id="{{$statement_entry->id}}[include]">
                                            </div>
                                        </td>
                                        <td class="text-nowrap">
                                            {{Carbon\Carbon::parse($statement_entry->spent_date)->format('Y-m-d')}}
                                            <input type="hidden" class="@error($statement_entry->id.'[spent_date]') is-invalid @enderror" name="expenses[{{$statement_entry->id}}][spent_date]" id="expenses[{{$statement_entry->id}}][spent_date]" value="{{Carbon\Carbon::parse($statement_entry->spent_date)->format('Y-m-d\TH:i:sO')}}">
                                            @error($statement_entry->id.'[spent_date]')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        <td>
                                            {{$statement_entry->client_name}}
                                            <input type="hidden" name="expenses[{{$statement_entry->id}}][client_name]" id="expenses[{{$statement_entry->id}}][client_name]" value="{{$statement_entry->client_name}}">
                                        </td>
                                        <td>{{$statement_entry->description}}</td>
                                        <td>
                                            {{$statement_entry->total_cost}}
                                            <input type="hidden" name="expenses[{{$statement_entry->id}}][total_cost]" id="expenses[{{$statement_entry->id}}][total_cost]" value="{{$statement_entry['total_cost']}}">
                                        </td>
                                        <td>
                                            <select id="expenses[{{$statement_entry->id}}][user_id]" name="expenses[{{$statement_entry->id}}][user_id]" class="form-control includable-input @error($statement_entry->id.'[user_id]') is-invalid @enderror" name="expenses[{{$statement_entry->id}}][user_id]" autocomplete="user_id[expenses[{{$statement_entry->id}}]]" autofocus required style="width:auto;" @if(!$statement_entry->include) disabled @endif>
                                                <option value="" selected disabled>Select employee</option>
                                                @foreach($employees as $employee)
                                                    <option value="{{$employee->id}}"  @if($statement_entry->user_id == $employee->id) selected @endif>{{$employee->name}}</option>
                                                @endforeach
                                            </select>
                                            @error($statement_entry->id.'[user_id]')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        <td>
                                            <select id="expenses[{{$statement_entry->id}}][project_id]" name="expenses[{{$statement_entry->id}}][project_id]"  class="form-control includable-input @error($statement_entry->id.'[project_id]') is-invalid @enderror" name="expenses[{{$statement_entry->id}}][project_id]" required autocomplete="[project_id][expenses[{{$statement_entry->id}}]]" autofocus style="width:auto;" @if(!$statement_entry->include) disabled @endif>
                                                <option value="" selected disabled>Select project</option>
                                                @foreach($projects as $project)
                                                    <option value="{{$project->id}}"  @if($statement_entry->project_id == $project->id) selected @endif @if(!$project->is_active) disabled @endif>{{$project->name}}</option>
                                                @endforeach
                                            </select>
                                            @error($statement_entry->id.'[project_id]')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        <td>
                                            <select id="expenses[{{$statement_entry->id}}][expense_category_id]" name="expenses[{{$statement_entry->id}}][expense_category_id]" class="form-control includable-input @error($statement_entry->id.'[expense_category_id]') is-invalid @enderror" required autocomplete="expenses[{{$statement_entry->id}}][expense_category_id]" autofocus style="width:auto;" @if(!$statement_entry->include) disabled @endif>
                                                <option value="" selected disabled>Select expense category</option>
                                                @foreach($expense_categories as $expense_category)
                                                    <option value="{{$expense_category->id}}" @if($statement_entry->expense_category_id == $expense_category->id) selected @endif>{{$expense_category->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('expense_category_id[]')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </td>
                                        <td>
                                            <div class="col ml-2">
                                                <input type="checkbox" class="billable_checkbox includable-input checkbox-inline @error($statement_entry->id.'[billable]') is-invalid @enderror" id="expenses[{{$statement_entry->id}}][billable_checkbox]" @if($statement_entry->billable) checked @endif @if(!$statement_entry->include) disabled @endif>
                                                @error($statement_entry->id.'[billable]')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <input type="hidden" name="expenses[{{$statement_entry->id}}][billable]" value="{{$statement_entry->billable? 1 : 0}}" id="{{$statement_entry->id}}[billable]">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col ml-2">
                                                <input type="checkbox" class="default_checkbox checkbox-inline @error($statement_entry->id.'[default]') is-invalid @enderror" id="expenses[{{$statement_entry->id}}][default_checkbox]">
                                                @error($statement_entry->id.'[default]')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <input type="hidden" name="expenses[{{$statement_entry->id}}][default]" value="0" id="{{$statement_entry->id}}[default]">
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group d-flex flex-row-reverse">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">
                                Add expenses
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    //Billable or Non-billable select switch. Shows one, hides the other
    $('.billable_checkbox').change(function() {
        if(this.checked) $(this).next('input').val('1');
        else $(this).nextAll('input').first().val('0');
    });
    // Include checkbox switch
    $('.include_checkbox').change(function() {
        $closest_tr = $(this).closest('tr');
        $closest_tr.find('.includable-input').prop('disabled', !this.checked);
        if(this.checked) {
            $(this).next('input').val('1');
        }
        else {
            $(this).nextAll('input').first().val('0');
        }
    });
    $('.default_checkbox').change(function() {
        if(this.checked) $(this).next('input').val('1');
        else $(this).nextAll('input').first().val('0');
    });
</script>
@stop
