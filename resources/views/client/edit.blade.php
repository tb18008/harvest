@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"><b>Edit client '{{$client->name}}'</b></div>

                    @include('messages')

                    <form class="m-2" method="POST" action="{{ route('clients.update',['client'=>$client])}}">

                        @csrf
                        @method('patch')

                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-right">Client name</label>

                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$client->name}}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group d-flex flex-row-reverse">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update client
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
